===========
 Changelog
===========

.. contents:: Versions
  :depth: 1

1.0.0 (2020-06-19)
==================

Features
--------
* **authentication:** optional LDAP authentication with env variables (`d19ec54 <https://gitlab.com/bbbatscale/bbbatscale/commit/d19ec54ea81621cfcdd111d0595c6ce5a7a724f1>`_)
* **core:** removed h_da dependencies, reverted home.js to old function, added default favicon.ico, added context_processors.py to access general parameter from everywhere (`e3aefe2 <https://gitlab.com/bbbatscale/bbbatscale/commit/e3aefe257a0c799777d5a4c346460d9e4e464b65>`_)
* **core.chat:** added chat to general parameters, re-compiled po files (`827a9ae <https://gitlab.com/bbbatscale/bbbatscale/commit/827a9ae9c87c2b9e7bd49ca98a7d514bc1ff8911>`_)
* **core.users:** added user crud, user password change option, fixed overview sides (`82d7079 <https://gitlab.com/bbbatscale/bbbatscale/commit/82d707957db0187882f9a2c0d44a9a9f18f1d4c9>`_)
* **flake8:** end line and unused import removed (`93d3950 <https://gitlab.com/bbbatscale/bbbatscale/commit/93d3950da38aab3fc788e07ea1847d9abf1b1fcd>`_)
* **localization:** switch between languages enabled, compiling languages inside docker (`c9da794 <https://gitlab.com/bbbatscale/bbbatscale/commit/c9da7945b25bac652b3ffec5ade970bf29e7b690>`_)

Bug Fixes
---------
* **authentication:** removed if and line endings for flake8 (`3605a88 <https://gitlab.com/bbbatscale/bbbatscale/commit/3605a884dde173419cd06f86c2876bcb483837fa>`_)
* **docker-compose:** renaming ConfMaster to BBBatScale (`58a8621 <https://gitlab.com/bbbatscale/bbbatscale/commit/58a86216d523b01aa2172c9b62f041d4bce3e50b>`_)
* **flake8:** fixed flake8 errors (`b8c6c93 <https://gitlab.com/bbbatscale/bbbatscale/commit/b8c6c933233e347d6203eb92fc6e96a7e251d4b6>`_)
* **gitlab-ci:** removed staticfiles from sonar analytics and removed old openshift steps (`5d1db5c <https://gitlab.com/bbbatscale/bbbatscale/commit/5d1db5cf40e044b2323abf32b13a364e9dc58af3>`_)
* **locale:** removed ConfMaster naming in PO files (`38bb535 <https://gitlab.com/bbbatscale/bbbatscale/commit/38bb53502beb02f0485beb6f514fe2000f353f51>`_)
* **locale:** removed ConfMaster naming in PO files (`fe4434b <https://gitlab.com/bbbatscale/bbbatscale/commit/fe4434b95a329cc107bd0bface769aadab4318b0>`_)
* **locale:** Typo in translation (`673903d <https://gitlab.com/bbbatscale/bbbatscale/commit/673903ddf5f03d74cb008ce2cbad68368053b5a4>`_)
* **migrations:** load initial data in migration (`8ffd424 <https://gitlab.com/bbbatscale/bbbatscale/commit/8ffd42423eb3776c49b7f9c90114fbcc44e6029b>`_)
* **template:** remove faq link if not set in general parameter (`8128dc2 <https://gitlab.com/bbbatscale/bbbatscale/commit/8128dc24b4938a045af6e2bf9e1d99068c83139e>`_)
* **translation:** removed fuzzy translations since they where not used, added compilemessages to docker entry (`562a30e <https://gitlab.com/bbbatscale/bbbatscale/commit/562a30ecc163e1b57f8cf85cdeb2193517fa1124>`_)
* **webhooks:** fix that web hooks got translated and prepended the language code (`ca9c3f8 <https://gitlab.com/bbbatscale/bbbatscale/commit/ca9c3f8375dbc31a8e132561279cf3e593a8dedd>`_)
* **webhooks:** removed i18n patterns since they did not work with webhooks (`71b7ea0 <https://gitlab.com/bbbatscale/bbbatscale/commit/71b7ea04f7ba5f34e2595abd1ebfe08813a8b389>`_)

Other
-----
* **docker:** copy licenses folder in docker (`586999b <https://gitlab.com/bbbatscale/bbbatscale/commit/586999be60b81e07700418fe05b4a21bc507ee25>`_)
* **env_variables:** extract environment variables out of the docker-compose files into dummy .env files (`dd9dca6 <https://gitlab.com/bbbatscale/bbbatscale/commit/dd9dca69960b7a5e4f4a188803b6579e31e21993>`_)
* **flake8:** ignore flake8 C901 in utils.py and views.py (`1ef52c2 <https://gitlab.com/bbbatscale/bbbatscale/commit/1ef52c286fe2f317a1ab54565324afb2ececbfc5>`_)
* **flake8:** ignore flake8 F403 in development.py, production.py and staging.py (`42b0408 <https://gitlab.com/bbbatscale/bbbatscale/commit/42b0408b581a85100f2eb4e530da00a0eb2fa060>`_)
* **gitignore:** add multiple entries to .gitignore including virtual environments (`fa84249 <https://gitlab.com/bbbatscale/bbbatscale/commit/fa842491447267a0c236f781b276f2b253c84298>`_)
* **gitlab_ci:** remove allow_failure from job 'flake8-checking' (`7fd6806 <https://gitlab.com/bbbatscale/bbbatscale/commit/7fd6806b9cc707b0bc89d0e523eaaba350955296>`_)
* **gitlab_ci:** reset entrypoint in step 'flake8-checking' and 'sonarqube' (`b1f3838 <https://gitlab.com/bbbatscale/bbbatscale/commit/b1f38386b1c999227519e5c7e5a9792c15a59712>`_)
* **locale:** Update on new translations (`391db58 <https://gitlab.com/bbbatscale/bbbatscale/commit/391db58c81a5febe46d9854622126f00f80b5404>`_)
* **manage.py:** changed default value to development settings (`b3ce158 <https://gitlab.com/bbbatscale/bbbatscale/commit/b3ce15829a383bde060172821b70ef210d093a9d>`_)
* **open-shift:** removed template files (`881861b <https://gitlab.com/bbbatscale/bbbatscale/commit/881861b8094a2f4a80247f9e96942c71579fc19a>`_)
* **project:** initial commit version 0.9.0 (`1ce8d64 <https://gitlab.com/bbbatscale/bbbatscale/commit/1ce8d6495d45b05e90cf9833de9acff0d3a6d380>`_)
* **settings:** extract the BASE_URL out of the settings into a environment variable (`f7ca422 <https://gitlab.com/bbbatscale/bbbatscale/commit/f7ca422775e758b8c91da5ba03ee0e63ef1be487>`_)
* **sonar:** update to new sonar project (`ed38eb0 <https://gitlab.com/bbbatscale/bbbatscale/commit/ed38eb0942e6ca094d5d8a2389cdcd952a25d167>`_)

`1.1.0 <https://gitlab.com/bbbatscale/bbbatscale/compare/1.0.0...1.1.0>`_ (2020-10-11)
================================================================================================================

Features
--------
* **auth:** drop custom auth backend for django-auth-ldap (`ce6a6dd <https://gitlab.com/bbbatscale/bbbatscale/commit/ce6a6dd00316c304cb98d7a49fc8b51c8388a531>`_)
* **ci:** support semantic release + push to hub.docker.com (`8b62855 <https://gitlab.com/bbbatscale/bbbatscale/commit/8b62855a23e512427a4c4b3e0c33466336b0b9b1>`_), closes `#43 <https://gitlab.com/bbbatscale/bbbatscale/issues/43>`_
* **guestpolicy:** Added GuestPolicy for meeting to enable guestlobby (`30f5529 <https://gitlab.com/bbbatscale/bbbatscale/commit/30f5529d942a24c24e1881931ab197686b06d54f>`_)
* **homerooms:** added frontend visualization to home.html (`0af757d <https://gitlab.com/bbbatscale/bbbatscale/commit/0af757db1bd0a765133c00c891aaaf8d99db9b90>`_)
* **homerooms:** changes in home room general parameters are synchronized to home rooms (`f8c9acf <https://gitlab.com/bbbatscale/bbbatscale/commit/f8c9acf2e5b6e47b1e9324591e9383c4285e4a07>`_)
* **homerooms:** home rooms can only be opened by owner (`957a5d6 <https://gitlab.com/bbbatscale/bbbatscale/commit/957a5d667a727c98e45c2021d02c6a9ed763ff22>`_)
* **homerooms:** names of home rooms are email instead of username (`3d8f8cf <https://gitlab.com/bbbatscale/bbbatscale/commit/3d8f8cfe51db72a08b3f1e101129919439a4ed29>`_)
* **homerooms:** rooms are created with user creation (according to general parameters) (`e9c7655 <https://gitlab.com/bbbatscale/bbbatscale/commit/e9c76557ae9b17fe0ac4ff77b8738fd5f539517e>`_)
* **lms/moodle:** adds basic moodle integration for bbb@scale (`463f28d <https://gitlab.com/bbbatscale/bbbatscale/commit/463f28d4f337186e6dafca1536bce4819ee0e5a9>`_)
* **local:** adds Galician translations (`e48a9ca <https://gitlab.com/bbbatscale/bbbatscale/commit/e48a9ca5c0e774e9a81a205edb0f65b31b3c2e7d>`_)
* **management:** sync ldap emails to user accounts script (`19c3348 <https://gitlab.com/bbbatscale/bbbatscale/commit/19c33480f9849509b45fdd0e690d98cbd4d3a616>`_)
* **pytest:** change from django test to pytest (`4b51f7c <https://gitlab.com/bbbatscale/bbbatscale/commit/4b51f7cd6dc6002b25b254e0a9264d90452dd1ed>`_)
* **support_chat:** add a feature toggle to the support_chat (`9286672 <https://gitlab.com/bbbatscale/bbbatscale/commit/928667254457546fa1232d091e09d8e239ecafe4>`_)
* **support_chat:** add a spectator mode to the supporter chat (`749e42e <https://gitlab.com/bbbatscale/bbbatscale/commit/749e42ed8746adc17c8f7d3994a63ea314e21f29>`_)
* **support_chat:** add ability for displaying a logo with notifications (`96209d0 <https://gitlab.com/bbbatscale/bbbatscale/commit/96209d004fa7ff81fc2f272523e4b48355cecd55>`_)
* **support_chat:** add first tests for the ChatConsumer, fix some bugs and improve code (`820d1c5 <https://gitlab.com/bbbatscale/bbbatscale/commit/820d1c53b74670bfe8a18de0c1f7b9dd1767f30e>`_)
* **support_chat:** add first tests for the ChatMetaConsumer, fix some bugs and improve code (`ea4fc8d <https://gitlab.com/bbbatscale/bbbatscale/commit/ea4fc8defa132f6bcc212cb50049203cabe8ef02>`_)
* **support_chat:** add first tests for the SupportConsumer, fix some bugs and improve code (`14a64c3 <https://gitlab.com/bbbatscale/bbbatscale/commit/14a64c33a367592ac04747099d3dd0a0c499e2c0>`_)
* **support_chat:** add localization (`d5f9a1e <https://gitlab.com/bbbatscale/bbbatscale/commit/d5f9a1e3ae4ae197f5c56965be8a46e0cfda69c5>`_)
* **support_chat:** add the ability for multiline messages (`4f6aeba <https://gitlab.com/bbbatscale/bbbatscale/commit/4f6aebaff125b303485236f1e3ab923ac38a2742>`_)
* **support_chat:** allow specifying password for redis access (`7fd129a <https://gitlab.com/bbbatscale/bbbatscale/commit/7fd129a5f479643a45c35889d4d7890454b899cb>`_)
* **support_chat:** describe REDIS_PASSWORD variable in django.env.dummy (`c549c79 <https://gitlab.com/bbbatscale/bbbatscale/commit/c549c79ecbd8d76ae72d40ff878287c515de7594>`_)
* **support_chat:** ensure messages are at most 250 characters long (`ba4f03a <https://gitlab.com/bbbatscale/bbbatscale/commit/ba4f03a30483590116617d81a1a0039404712aa4>`_)
* **support_chat:** ensure messages order and load history bit by bit (`d96e05b <https://gitlab.com/bbbatscale/bbbatscale/commit/d96e05bc35230a5f8ab8b34094179df23a0b3120>`_)
* **support_chat:** fully functional support status signal light and reduced chats (`c794999 <https://gitlab.com/bbbatscale/bbbatscale/commit/c794999d15ae38071576c32a89cbb8b10a7caed5>`_)
* **support_chat:** initial commit of the support chat (`5b626e1 <https://gitlab.com/bbbatscale/bbbatscale/commit/5b626e118caca83c9bc4760de650fadecc6068c0>`_), closes `#15 <https://gitlab.com/bbbatscale/bbbatscale/issues/15>`_
* **support_chat:** remodel chat/support UI (`1e8d7a0 <https://gitlab.com/bbbatscale/bbbatscale/commit/1e8d7a07f606ced8d7b38bc8843efdc2e104e842>`_)

Bug Fixes
---------
* **auth:** parsing correct - first - email from ldap entry object (`cfef4dd <https://gitlab.com/bbbatscale/bbbatscale/commit/cfef4ddf39b459fb07049ad0ca430fecc1a56ee7>`_)
* **flake8:** line to long (`3fc4458 <https://gitlab.com/bbbatscale/bbbatscale/commit/3fc4458655d6767133ea9de47f2fd7be4bfc7623>`_)
* **flake8:** style change (`b989737 <https://gitlab.com/bbbatscale/bbbatscale/commit/b989737f1fdfa5dfce2ec5d333c916bd5cf114df>`_)
* **gl django.po:** adds new translations for gl (`faa72e0 <https://gitlab.com/bbbatscale/bbbatscale/commit/faa72e0d4f174a7b95b9e961b7f8729bba1f1ce1>`_)
* **homerooms:** change owner one-to-one relationship to home room (`cc4563d <https://gitlab.com/bbbatscale/bbbatscale/commit/cc4563d4e37a569deeaaa2bbbfa58ad73bff7cb1>`_)
* **homerooms:** Creation for non-teachers fixed (according to general parameter) (`4242de5 <https://gitlab.com/bbbatscale/bbbatscale/commit/4242de5e6a08ee7744464627a2b81c4c7b0ff3dd>`_)
* **homerooms:** fix "allow guests" for home rooms (`7b9817c <https://gitlab.com/bbbatscale/bbbatscale/commit/7b9817c8f9f2cb7929b6821f483fe3e0e15f5ada>`_)
* **homerooms:** flake8 *facepalm* (`e24f859 <https://gitlab.com/bbbatscale/bbbatscale/commit/e24f8595e7db1051d95dad19c0b5478384d833a4>`_)
* **homerooms:** now also available for superuser (`0bb4785 <https://gitlab.com/bbbatscale/bbbatscale/commit/0bb4785a39fedf9e620b732570d7bb768a40fd9e>`_)
* **include:** flake8 unused include removed (`8fcb2db <https://gitlab.com/bbbatscale/bbbatscale/commit/8fcb2db21c6ac4c0471914fcdc7ee0bc8bfc5664>`_)
* **join-create-rooms:** check of related child models for rooms fixed (`45e7162 <https://gitlab.com/bbbatscale/bbbatscale/commit/45e71628ae7b36ec06c632eaf4264cfd466d00f6>`_)
* **ldap:** Get mail from ldap to user (`f54c94c <https://gitlab.com/bbbatscale/bbbatscale/commit/f54c94c57d92df6607e599cd98774c7bf2c4d233>`_)
* **localization:** extract `format()`:code: out of `gettext()`:code: (`5a7bdc8 <https://gitlab.com/bbbatscale/bbbatscale/commit/5a7bdc8a806f21437e82f66a054e4cf27c4502c1>`_)
* **migrations:** adds missed migration of latest changes (`d5f5f5e <https://gitlab.com/bbbatscale/bbbatscale/commit/d5f5f5e444cae521df3dc2be7a55582502be17f7>`_)
* **models:** Fixed typo in help_text of field home_room_enabled (`9489b7d <https://gitlab.com/bbbatscale/bbbatscale/commit/9489b7ddb19b4fa1889d11b43911d1dc2fc072e6>`_)
* **pipeline:** adjust the pipeline to be able to run without external secrets (`9fb21b5 <https://gitlab.com/bbbatscale/bbbatscale/commit/9fb21b5101685ddf1c911be5d1b9bfed6ee87975>`_)
* **pipeline:** copy container image to 'latest' tag (`1577b79 <https://gitlab.com/bbbatscale/bbbatscale/commit/1577b7957a7fbb8b0d6073815ed65bc812a4446d>`_)
* **pipeline:** copy container image to 'latest' tag (`afb37ce <https://gitlab.com/bbbatscale/bbbatscale/commit/afb37cec345522d481b8cf4b9c34aa3d512d33c2>`_)
* **pipeline:** use crane instead of kaniko for image tagging (`3137e67 <https://gitlab.com/bbbatscale/bbbatscale/commit/3137e67d1f879f6004749283fb70574208cb8559>`_)
* **reverse api:** fixes recent breaking bbb-moodle-plugin api call changes (`2562aa7 <https://gitlab.com/bbbatscale/bbbatscale/commit/2562aa777984a56025e7255d5b2d6520ccad9700>`_)
* **services:** fix broken url redirect for h-da data (`9bc5c17 <https://gitlab.com/bbbatscale/bbbatscale/commit/9bc5c178c7db00034ed68eb228ebe5bc8060d3de>`_)
* **services:** fix flake8 errors... my bad... (`dd17a00 <https://gitlab.com/bbbatscale/bbbatscale/commit/dd17a00ca793d8a48ff032c3d9d6fc57b09df60e>`_)
* **services:** remove possible unnecessary `<br/>`:code:\s in `moderator_message`:code: and use correct base URL in `create_web_hook_bbb`:code: (`04a3e17 <https://gitlab.com/bbbatscale/bbbatscale/commit/04a3e17b78db2b21b480213152df117c45050db2>`_)
* **services:** update localizations and replace hard coded url with django.urls.reverse (`da3ea00 <https://gitlab.com/bbbatscale/bbbatscale/commit/da3ea002600d50d1706909b57fd5622ed36b523b>`_)
* **style:** flake8 (`95227c0 <https://gitlab.com/bbbatscale/bbbatscale/commit/95227c05bbfb1ba0e206e9861a38bd1d4a62174b>`_)
* **support_chat:** fix chat.owner fetched outside of `database_sync_to_async`:code: (`ddc4230 <https://gitlab.com/bbbatscale/bbbatscale/commit/ddc4230b008781ecc1aa9aa113a86801288bfe5d>`_)
* **support_chat:** fix not loading new messages bug and some (scroll) animation bugs add notifications (`0b0e578 <https://gitlab.com/bbbatscale/bbbatscale/commit/0b0e578bee9405e5efe6602258a28063b9d0e927>`_)
* **support_chat:** fix supporter not marking chat as read after join without further actions (`c2c42e7 <https://gitlab.com/bbbatscale/bbbatscale/commit/c2c42e79896853f9601a4e8c91d5f0db435fa8a3>`_)
* **translation:** Added Translations for guestlobby (`643a6ae <https://gitlab.com/bbbatscale/bbbatscale/commit/643a6aef931240cd2b8c1a3a40c9603bcac47e90>`_)
* **translations:** remove obsolete space (`f24b8a1 <https://gitlab.com/bbbatscale/bbbatscale/commit/f24b8a1d4346853681b65957e2cbe83c57d37aae>`_)
* **typo:** #Autsch (`9b4a7a9 <https://gitlab.com/bbbatscale/bbbatscale/commit/9b4a7a98c5f992f76b9c01dac90c55a5af37b803>`_)
* **urls:** accept all room names not only `[^/]+/[^/]+`:code: and `[^/]+`:code: (regex) (`b725ead <https://gitlab.com/bbbatscale/bbbatscale/commit/b725ead233ed3ef23b03f9bc77692979e23aa89e>`_)
* fixing a bug in the dark theme (`26cf19f <https://gitlab.com/bbbatscale/bbbatscale/commit/26cf19fa778faedb67357bd4a8292376fae12d90>`_), closes `#42 <https://gitlab.com/bbbatscale/bbbatscale/issues/42>`_
* small adjustments to the darktheme (`e8a6460 <https://gitlab.com/bbbatscale/bbbatscale/commit/e8a64608ff30dc4c2826e0cfa1b8f812a1c5f953>`_), closes `#42 <https://gitlab.com/bbbatscale/bbbatscale/issues/42>`_

Other
-----
* **base_html:** replace the form select of the language selection with a 'form'-less dropdown (`bb988ab <https://gitlab.com/bbbatscale/bbbatscale/commit/bb988ab6d765cee4344f78a5f57a663169155bbf>`_)
* **ci:** replace the default releasetag format with the custom one "" (`c3f0842 <https://gitlab.com/bbbatscale/bbbatscale/commit/c3f084287cbbe6df092b15f064ce69a34f9f9757>`_)
* **docker:** change volume name to prevent collisions (`c328d20 <https://gitlab.com/bbbatscale/bbbatscale/commit/c328d20f2c6b508924561ed951ff4110081e7349>`_)
* **docker:** move env variables to dummy files (`378b122 <https://gitlab.com/bbbatscale/bbbatscale/commit/378b1222cfbcecc8339f89658cbde6f1395dca79>`_)
* **docker:** update python to 3.8.5 and removed nginx service in the developer docker-compose.yml (`f63d762 <https://gitlab.com/bbbatscale/bbbatscale/commit/f63d7625b32faf82db603677c226f382c799948c>`_)
* **feature:** added get_join_password with moderator attendace (`2f51f20 <https://gitlab.com/bbbatscale/bbbatscale/commit/2f51f20ce9c98cfe80a79da7016d8e84f5331a46>`_)
* **import_export:** add missing fields to the json import export feature (`156a9fd <https://gitlab.com/bbbatscale/bbbatscale/commit/156a9fd9d97d5c82494602e8d45025d17af4e99a>`_)
* **migration:** merge leaf nodes in the migration graph (`dbd66b9 <https://gitlab.com/bbbatscale/bbbatscale/commit/dbd66b9ac8218820f6afff0d405f8bfc1012571c>`_)
* **pipeline:** move pytest report options into the pipeline (`47e6a36 <https://gitlab.com/bbbatscale/bbbatscale/commit/47e6a360bf5081a702d160ff85fee0940dc7db2f>`_)
* **pipeline:** speed up pipeline by using `needs`:code: (`cf0cf11 <https://gitlab.com/bbbatscale/bbbatscale/commit/cf0cf11c35026622f9de42f0cc399fd4f6052003>`_)
* **pipeline:** upload pytest report as GitLab junit artifact (`e3df718 <https://gitlab.com/bbbatscale/bbbatscale/commit/e3df7184568d6e5c3847509d4120a02507450e0d>`_)
* **requirements:** update requirements.txt (`9d050d9 <https://gitlab.com/bbbatscale/bbbatscale/commit/9d050d918ec952e44c3d2fb60bf54c0e3dbe7cce>`_)
* **requirements:** update versions (`6980089 <https://gitlab.com/bbbatscale/bbbatscale/commit/69800897fc471fcd6f05b4650253d2a3298fe671>`_)
* **services:** add todos to unfinished localizations (`f0157c9 <https://gitlab.com/bbbatscale/bbbatscale/commit/f0157c96af79bf0faafae7355875487ce6e0823b>`_)
* **sonar:** update to new sonar url (`09eb470 <https://gitlab.com/bbbatscale/bbbatscale/commit/09eb470e35d02cac429216aeb64811fe3b548aa5>`_)
* **spelling:** Comply with naming conventions Update README.md (`7563b10 <https://gitlab.com/bbbatscale/bbbatscale/commit/7563b1019a6523850270f051f61ae835a749c251>`_)
* **support_chat:** change `locales`:code: of `Intl.DateTimeFormat`:code: to `undefined`:code: in order to let the browser decide the locale (`fdbeb3c <https://gitlab.com/bbbatscale/bbbatscale/commit/fdbeb3c4069032ed06e5ecc11e7af2c0625c37c7>`_)
* **support_chat:** dissolve the ChatMeta WebSocket and add atomic transactions (`86f4bbf <https://gitlab.com/bbbatscale/bbbatscale/commit/86f4bbfdeb067b00d6689cb909b60d7fd5dfecaf>`_)
* **support_chat:** fix bug and expand test (`16e74c0 <https://gitlab.com/bbbatscale/bbbatscale/commit/16e74c077c0c2cc14b4e4f6183cd477149e4f910>`_)
* **support_chat:** refactor the websockets' receive_json in order to extract the commands into their own methods (`3baa845 <https://gitlab.com/bbbatscale/bbbatscale/commit/3baa8455f9672716ebe1c5dda0912ba9f474cadb>`_)
* **support_chat:** rename support view and only accept `enabled`:code: for the env variable `SUPPORT_CHAT`:code: (`e5ba81a <https://gitlab.com/bbbatscale/bbbatscale/commit/e5ba81a420a4e3a5e28f8dbaa0937c6161bed415>`_)
* **support_chat:** split websocket_urlpatterns and make support_chat independent from core and BBBatScale (`54fcd15 <https://gitlab.com/bbbatscale/bbbatscale/commit/54fcd15c9ca4905394c3d1695c0bbf1a8f9b9a5d>`_)
* **templates:** reformat files and refactor some JavaScript functions (`3190a49 <https://gitlab.com/bbbatscale/bbbatscale/commit/3190a495396d5442beaec649c05a253de01c0513>`_)
* Added source code for dark theme (`239c6dd <https://gitlab.com/bbbatscale/bbbatscale/commit/239c6ddaad89a38fc5dbf04f03afe55bbaf768d1>`_), closes `#42 <https://gitlab.com/bbbatscale/bbbatscale/issues/42>`_

`1.1.1 <https://gitlab.com/bbbatscale/bbbatscale/compare/1.1.0...1.1.1>`_ (2020-10-11)
================================================================================================================

Bug Fixes
---------
* **export:** serializable error for json export (`70deacc <https://gitlab.com/bbbatscale/bbbatscale/commit/70deacc47bce1868792c0a1c1b5ee0c813ef3395>`_)

`1.1.2 <https://gitlab.com/bbbatscale/bbbatscale/compare/1.1.1...1.1.2>`_ (2020-10-11)
================================================================================================================

Bug Fixes
---------
* **flak8:** import error (`3003527 <https://gitlab.com/bbbatscale/bbbatscale/commit/30035277da8775d1cc40dc750cb8d0e05abe5d78>`_)

`1.1.3 <https://gitlab.com/bbbatscale/bbbatscale/compare/1.1.2...1.1.3>`_ (2020-10-11)
================================================================================================================

Bug Fixes
---------
* user import json added (`b7fb71e <https://gitlab.com/bbbatscale/bbbatscale/commit/b7fb71e7c47a045207794107b794a7345037bb51>`_)

`1.1.4 <https://gitlab.com/bbbatscale/bbbatscale/compare/1.1.3...1.1.4>`_ (2020-10-12)
================================================================================================================

Bug Fixes
---------
* meeting import fix (`ea65c3e <https://gitlab.com/bbbatscale/bbbatscale/commit/ea65c3ed0a4d3074b06063f7d12938edc005e9ab>`_)

`2.0.0 <https://gitlab.com/bbbatscale/bbbatscale/compare/1.1.4...2.0.0>`_ (2020-10-12)
================================================================================================================

⚠ BREAKING CHANGES
------------------
* **model:** Introduce custom user model. Major db schema change that will break installations
* **model:** Introduce custom user model. Major db schema change that will break installations

Features
--------
* **api:** added recoding handling for external LMS, added parameter dialNumber, welcome, logoutUrl for room configuration, cleanup (`840d49f <https://gitlab.com/bbbatscale/bbbatscale/commit/840d49f27bbab5856659a44252167664c6cf4933>`_)
* **homerooms:** users can change settings of their home room (`9a7fe3d <https://gitlab.com/bbbatscale/bbbatscale/commit/9a7fe3dd96c132050fdc86eb1929e1b9ca3e4a1c>`_)
* **jitsi:** start jitsi button is now infobox on frontpage (`4460efd <https://gitlab.com/bbbatscale/bbbatscale/commit/4460efd4c6a8765e394b7ee19cc4d77c1e110940>`_)
* **jitsi_int:** allow jitsi ad-hoc rooms to be deactivated (`436840c <https://gitlab.com/bbbatscale/bbbatscale/commit/436840cfa1ee9bba5a77094a9bb9915086c386ae>`_)
* **jitsi_int:** allow jitsi ad-hoc rooms to be deactivated (`4d913cc <https://gitlab.com/bbbatscale/bbbatscale/commit/4d913ccfb0158842dfaa80113b9d4cf04933da2e>`_)
* **model:** introduce custom user model (`160224a <https://gitlab.com/bbbatscale/bbbatscale/commit/160224a409a8d62f23d28ac8d71b67feb6c08538>`_)
* **model:** introduce custom user model (`cbd9010 <https://gitlab.com/bbbatscale/bbbatscale/commit/cbd901091557e466d7f43bcb0287c3d979c97f80>`_)
* **personalrooms:** personal rooms can be created and deleted (`d3b75c2 <https://gitlab.com/bbbatscale/bbbatscale/commit/d3b75c2d2b3947c376b585150b55d7289a93ffd7>`_)
* **personalrooms:** start personal rooms only as owner or co-owner (`22cf756 <https://gitlab.com/bbbatscale/bbbatscale/commit/22cf756b88c50bcf4804006c694832ad2f609527>`_)
* **personalrooms:** useraccount is not shown on UI (`dbada81 <https://gitlab.com/bbbatscale/bbbatscale/commit/dbada815545e894a47387a3d534be42bcdd20a6f>`_)
* **themes:** user bound theme settings (`73f147e <https://gitlab.com/bbbatscale/bbbatscale/commit/73f147e6277c89459627820a219d4143792a4d18>`_)
* added jquery multiselect with backend filter for co_onwner in personal rooms (`a5858c3 <https://gitlab.com/bbbatscale/bbbatscale/commit/a5858c3d5fa26f5ccdda0c0246986efa5f5bbb2f>`_)

Bug Fixes
---------
* **flake8:** don't want to talk about that... flake8 (`8ed0521 <https://gitlab.com/bbbatscale/bbbatscale/commit/8ed05216e322ab3428338c041f2997f70a357f3b>`_)
* **flake8:** fix all the nix errors that flake8 gave me... (`e4400e3 <https://gitlab.com/bbbatscale/bbbatscale/commit/e4400e343ad35759de8ea9d36fb0447ffb1632c8>`_)
* **personalrooms:** co-owners now visible for co-owner updat view (`4c5adac <https://gitlab.com/bbbatscale/bbbatscale/commit/4c5adacb36ed02157378c4160cb75bf2e49cff1b>`_)
* **personalrooms:** color theme of create room / maximum number of rooms (`e7e1b11 <https://gitlab.com/bbbatscale/bbbatscale/commit/e7e1b119b849a262dad0d745630c94c8bbc8d7cf>`_)
* **personalrooms:** fix create personal room issue cause by merge (`309eac1 <https://gitlab.com/bbbatscale/bbbatscale/commit/309eac17697ad0e901adc8e42ab2f3f2f043f345>`_)
* **personalrooms:** Max room button is not a button anymore (`9e66643 <https://gitlab.com/bbbatscale/bbbatscale/commit/9e666434f7f1c980dee2ccf7199e2477dc069172>`_)
* **personalrooms:** meeting id generation after creation of personal room generation. (`d95433e <https://gitlab.com/bbbatscale/bbbatscale/commit/d95433eef0b294d27a7de274b1bd04a380b7adef>`_)
* **personalrooms:** protect read only fields on ui against tampering (`79c38b3 <https://gitlab.com/bbbatscale/bbbatscale/commit/79c38b3253602b619017a99a1646086643eea08c>`_)
* **personalrooms:** update label to hint that email address is used (`0414cca <https://gitlab.com/bbbatscale/bbbatscale/commit/0414cca6d9e87a984c77c9c37b3c43b0d0a917bf>`_)
* **project:** renamed Instance to Server (`0a1c3e0 <https://gitlab.com/bbbatscale/bbbatscale/commit/0a1c3e0c9c2d0dadd8ace6538dc93070aecc9298>`_)
* **project:** renamed Instance to Server (`e19bffd <https://gitlab.com/bbbatscale/bbbatscale/commit/e19bffd48dd18c19ca1cafecf2170c6abb22305f>`_)
* **translations:** fix missed hard coded labels to translated labels (`7e336b2 <https://gitlab.com/bbbatscale/bbbatscale/commit/7e336b2d908f24a00341ae56d9410727dc0a3b90>`_)
* personalroom coowner edit form view overwriting with instance values (`127c70d <https://gitlab.com/bbbatscale/bbbatscale/commit/127c70da730ea01b342d118fe06040a48423eb49>`_)
* refactorings pen black, rename function is_homeroom (`8955d21 <https://gitlab.com/bbbatscale/bbbatscale/commit/8955d211b68b2e9d1b685e82a693bf1b05996867>`_)

Other
-----
* **model:** harmonize settings for personal rooms in general parameters (`6a9a56a <https://gitlab.com/bbbatscale/bbbatscale/commit/6a9a56a262e96944ebb91f8c3cf83179c4b8dee7>`_)
* **sonar:** fix some sonar issues (`691af85 <https://gitlab.com/bbbatscale/bbbatscale/commit/691af85fb861e4b513381ccce7d2c7f477451529>`_)
* **test_data:** remove h_da employee names and replace h-da.de urls with example.org (`07a149f <https://gitlab.com/bbbatscale/bbbatscale/commit/07a149f49affb6fa28be1313c9a65962b7c106e9>`_)
* **themes:** move default themes into migrations (`00c94e1 <https://gitlab.com/bbbatscale/bbbatscale/commit/00c94e127d225a6a6096de2ef64c63cf23c00576>`_)
* merged personals into 2.0.0 release (`6bd81ea <https://gitlab.com/bbbatscale/bbbatscale/commit/6bd81ea743ae08119db034a54ca1bf98dec5e12d>`_)
* merged personals into 2.0.0 release (`6958bb6 <https://gitlab.com/bbbatscale/bbbatscale/commit/6958bb6ad2a3d4dda257a538155da498d60843eb>`_)

`2.0.1 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.0.0...2.0.1>`_ (2020-10-12)
================================================================================================================

Bug Fixes
---------
* uvicorn version fixed (`722b69b <https://gitlab.com/bbbatscale/bbbatscale/commit/722b69b9cf51a6cbc89824575aeddd92943019b3>`_)

`2.0.2 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.0.1...2.0.2>`_ (2020-10-12)
================================================================================================================

Bug Fixes
---------
* hardcoded Group in User (`98ae68b <https://gitlab.com/bbbatscale/bbbatscale/commit/98ae68b989bddea582371bef0fb1b76961f8e648>`_)

`2.0.3 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.0.2...2.0.3>`_ (2020-10-12)
================================================================================================================

Bug Fixes
---------
* darktheme personalroom (`0f6d675 <https://gitlab.com/bbbatscale/bbbatscale/commit/0f6d675c16fb0045dd02fb821ea169d86acee1e4>`_)
* flask8 (`6186a0e <https://gitlab.com/bbbatscale/bbbatscale/commit/6186a0e05c981c02484f11e100dbaff1dcd96499>`_)
* personal room creation fixed (`b57eafd <https://gitlab.com/bbbatscale/bbbatscale/commit/b57eafd211a7de876815369f29d41f46d55616ff>`_)

`2.0.4 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.0.3...2.0.4>`_ (2020-10-12)
================================================================================================================

Bug Fixes
---------
* **import_export:** fix general parameter save (`6b1c359 <https://gitlab.com/bbbatscale/bbbatscale/commit/6b1c35917662ca93dd71a27e332e6da0726a4d90>`_)
* **personalrooms:** fix copy to clipboard for personal rooms (`efc1e4a <https://gitlab.com/bbbatscale/bbbatscale/commit/efc1e4a948bc27dad8c3d1c61be8726d1b1c3595>`_)

`2.0.5 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.0.4...2.0.5>`_ (2020-10-13)
================================================================================================================

Bug Fixes
---------
* closes `#85 <https://gitlab.com/bbbatscale/bbbatscale/issues/85>`_ (`ca25f6a <https://gitlab.com/bbbatscale/bbbatscale/commit/ca25f6a97852f0d83982db07bcc796ce2fdf8b87>`_)
* guest lobby fixed (`e8b2c32 <https://gitlab.com/bbbatscale/bbbatscale/commit/e8b2c32eed3dc4a8fc41e7557c343a88acd5623a>`_)
* house keeping transaction handling (`53438d2 <https://gitlab.com/bbbatscale/bbbatscale/commit/53438d2ed203cfe4802f1c7a0415c1c4c9e06e41>`_)
* transaction handling, collect instances stats, room creation without config (`e19faa8 <https://gitlab.com/bbbatscale/bbbatscale/commit/e19faa81a3bc9545f4f4c8829044b4c0f5d73102>`_)

`2.0.6 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.0.5...2.0.6>`_ (2020-10-19)
================================================================================================================

Bug Fixes
---------
* internal server error because of timeout (`e50e262 <https://gitlab.com/bbbatscale/bbbatscale/commit/e50e26295d76cd0c3eaee9ac88584614fc11727c>`_)

`2.1.0 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.0.6...2.1.0>`_ (2020-10-21)
================================================================================================================

Features
--------
* **pipeline:** wait for the sonarqube to finish and pass its result as pipeline job result (`9557f6a <https://gitlab.com/bbbatscale/bbbatscale/commit/9557f6a3c05a64cd1276f33c9e187ecb6e8148a4>`_)

Bug Fixes
---------
* **base.html:** translate logout button and add proper styling (`8a633fb <https://gitlab.com/bbbatscale/bbbatscale/commit/8a633fb82d631662d852deeb0fc85bc96c9fca5b>`_)
* **migrations:** correct some `help_text`:code:\s and `verbose_name`:code:\s (`4fad7e5 <https://gitlab.com/bbbatscale/bbbatscale/commit/4fad7e5b4e4dc242dec36455b127f1daab739968>`_)
* **support_chat:** add missing `default_app_config`:code: variable to `support_chat/__init__.py`:code: (`4fcc4cc <https://gitlab.com/bbbatscale/bbbatscale/commit/4fcc4ccbbef5ed60ef5d0f0aad7ac1d809cf5634>`_)

Other
-----
* **base.html:** move scripts into the head (`dd50989 <https://gitlab.com/bbbatscale/bbbatscale/commit/dd50989aa0c6f3954ad3c93a91f91a60875004fa>`_)
* **base.html:** remove changeLanguage function and replace with concrete `form`:code:\s (`87ef65d <https://gitlab.com/bbbatscale/bbbatscale/commit/87ef65d8eb673314f8f30504118e2f937431366b>`_)
* **group_required:** remove unnecessary `group_required`:code: decorator (`ccf22d2 <https://gitlab.com/bbbatscale/bbbatscale/commit/ccf22d234306f560af1b84df85966e21cbf4af47>`_)
* **json_search_user:** rename keywords in the json response of the `json_search_user`:code: view (`6e07d2c <https://gitlab.com/bbbatscale/bbbatscale/commit/6e07d2c3c150fdb5e090b5769ed98efc0beff6f5>`_)
* **oidc:** remove `IS_OIDC_ENABLED`:code: template tag and add `oidc_enabled`:code: as context processors (`3083957 <https://gitlab.com/bbbatscale/bbbatscale/commit/3083957710a8b0b6d663e07d75823ace3eebbe83>`_)
* **rooms:** reduce database selects for rooms by prefetching configs, homerooms, personalrooms (`7e1eeba <https://gitlab.com/bbbatscale/bbbatscale/commit/7e1eebadb4cf02a584e39c4c558bc3b4817bdf04>`_)
* **support_chat:** send timestamp in iso format instead of epoch millis (`ff03bed <https://gitlab.com/bbbatscale/bbbatscale/commit/ff03beda78db71427235d200b1f2af36a6b4fea8>`_)
* **tab_view:** extract 'tab-view' from the support overview into its own reusable feature (`3d44381 <https://gitlab.com/bbbatscale/bbbatscale/commit/3d443817adfbcd1f34b760c33f51053ff360fc7d>`_)
* **urls:** move decorator `login_required`:code: from urls.py to views.py (`80dabe3 <https://gitlab.com/bbbatscale/bbbatscale/commit/80dabe3928760e11a85e2754b4fd0e127be09b30>`_)
* **user_model:** remove `user_get_real_name`:code: template tag and modify `User.__str__`:code: correspondingly (`2433b05 <https://gitlab.com/bbbatscale/bbbatscale/commit/2433b054b0d6bb77ec86c554080a711144e9e134>`_)
* **utilities.js:** move `chat/js/utilities.js`:code: to `js/utilities.js`:code: (`8cbcfe7 <https://gitlab.com/bbbatscale/bbbatscale/commit/8cbcfe7541a44b104642903440b91fc88fe96637>`_)
* **websockets:** move extended websocket base class to `utils/websockets`:code: (`8876939 <https://gitlab.com/bbbatscale/bbbatscale/commit/8876939e98a3df4daf667630c2d540c909299cb4>`_)

`2.1.1 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.1.0...2.1.1>`_ (2020-10-23)
================================================================================================================

Bug Fixes
---------
* fixed error where room was stuck in creating state (`568f3c5 <https://gitlab.com/bbbatscale/bbbatscale/commit/568f3c5530af4df3af000c704756a854f9c58cae>`_)
* fixed error where room was stuck in creating state (`5d94afc <https://gitlab.com/bbbatscale/bbbatscale/commit/5d94afc086b13ff2327c313bd7e34cd891ccb773>`_)
* participantCount was used as videostream count (`cceaa00 <https://gitlab.com/bbbatscale/bbbatscale/commit/cceaa00c99ab8f625007a244c667ec91f589b646>`_)
* participantCount was used as videostream count (`e7f4667 <https://gitlab.com/bbbatscale/bbbatscale/commit/e7f4667b33030d39a1567f6fecef94f076d3718a>`_)

Other
-----
* **services:** intends (`42969dd <https://gitlab.com/bbbatscale/bbbatscale/commit/42969ddad4a5c5320e5a00ffb6f31bcd25c4c1f9>`_)
* **view:** intends (`1dfb497 <https://gitlab.com/bbbatscale/bbbatscale/commit/1dfb497c53d199bc7dd108c621ab6faf75705bdc>`_)
* **view:** intends (`efaebd6 <https://gitlab.com/bbbatscale/bbbatscale/commit/efaebd64592597fce6cbf8cf2157133b9ea4e971>`_)
* refactored permission check before creating a meeting (`4502cba <https://gitlab.com/bbbatscale/bbbatscale/commit/4502cba73a46a0a1f0562a8d886fe6828e432e4c>`_)

`2.2.0 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.1.1...2.2.0>`_ (2020-10-23)
================================================================================================================

Features
--------
* **UI:** include max personal rooms in user view (`5e7b965 <https://gitlab.com/bbbatscale/bbbatscale/commit/5e7b96508d05aa8ee9c234749f45f6fe103d4b08>`_)

Bug Fixes
---------
* **recordings_api:** recordings are now correctly parsed (`128ecd4 <https://gitlab.com/bbbatscale/bbbatscale/commit/128ecd424822c0d74fb0cbaf4a4ac1c0a20e19ef>`_)
* **tests:** add missing fixtures for tests (`6987604 <https://gitlab.com/bbbatscale/bbbatscale/commit/698760450efad37ee76d604ac20632618a54d3df>`_)
* **trans:** added Missing Translations (`aa64c10 <https://gitlab.com/bbbatscale/bbbatscale/commit/aa64c109a434118355e44698e84bbed86d5b15a8>`_)
* **trans:** ffixed up tranlation (`5f0f127 <https://gitlab.com/bbbatscale/bbbatscale/commit/5f0f127d9dc9637dede48d35cc280de8daf4ab21>`_)

Other
-----
* translation ita refactored (`c94a95c <https://gitlab.com/bbbatscale/bbbatscale/commit/c94a95c7a0099dc25365500db9cd8745a1bb1d0b>`_)

`2.3.0 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.2.0...2.3.0>`_ (2020-10-25)
================================================================================================================

Features
--------
* **logging:** customize logging by provide logging_config.py, include json formatter, prepare graylog forwarding (`6eed481 <https://gitlab.com/bbbatscale/bbbatscale/commit/6eed481b1beee34bc39e30cff35e208962a3eb9b>`_)
* **logging:** include correlation id in logging (`af6e8e1 <https://gitlab.com/bbbatscale/bbbatscale/commit/af6e8e1852da911899afd9080ddf5036b3ee6f22>`_)

`2.4.0 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.3.0...2.4.0>`_ (2020-10-30)
================================================================================================================

Features
--------
* **support_chat:** add the possibility for a specific supporter group (`2c27c55 <https://gitlab.com/bbbatscale/bbbatscale/commit/2c27c558bd5dff2435283fc9e961b4af38de635e>`_)
* **webhooks:** retry failed webhooks (`a338531 <https://gitlab.com/bbbatscale/bbbatscale/commit/a3385319b08fa41320c801c57a2f55f4dfbc747a>`_)
* **webhooks:** run rqworker from docker-compose (`6a796b6 <https://gitlab.com/bbbatscale/bbbatscale/commit/6a796b6b2e3e9d87ce7af86d5af490f8e791ba34>`_)
* **webhooks:** signal INCOMING_MESSAGE event from support chat (`6ac546c <https://gitlab.com/bbbatscale/bbbatscale/commit/6ac546c21dd7a3f33076f60e724d8118d596c076>`_)
* **webhooks:** use Full Jitter for computing backoff delays (`ae8012a <https://gitlab.com/bbbatscale/bbbatscale/commit/ae8012ab2a5426f1f86bda2dd305504a212a9d12>`_)
* add machinery for webhook support (`09b9cc5 <https://gitlab.com/bbbatscale/bbbatscale/commit/09b9cc5f15fee0d3be7864fe4493b65ddf457e39>`_)
* handle maxparticipants (`6013b51 <https://gitlab.com/bbbatscale/bbbatscale/commit/6013b512466ba786e7f641443230021a1380abd1>`_)

Bug Fixes
---------
* **trans:** update german translation (`767703b <https://gitlab.com/bbbatscale/bbbatscale/commit/767703b0f093571d3a16b25538c2b5966db1db31>`_)
* **translation:** reworked german translations (`8d7b504 <https://gitlab.com/bbbatscale/bbbatscale/commit/8d7b504f1478456126161d4a04ac7bc6d494d27c>`_)

Other
-----
* **migrations:** reformat migration `0002_webhook`:code: (`b8cfce5 <https://gitlab.com/bbbatscale/bbbatscale/commit/b8cfce52ea5231038a27ef30885965d397445914>`_)
* **support_chat:** update type hints and rework fixtures (`1fec0bf <https://gitlab.com/bbbatscale/bbbatscale/commit/1fec0bfe12f16e1225f2d6e90e6e3644e4fed2d0>`_)

`2.4.1 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.4.0...2.4.1>`_ (2020-10-30)
================================================================================================================

Bug Fixes
---------
* **lang:** Fixing error in po file (`caaa3da <https://gitlab.com/bbbatscale/bbbatscale/commit/caaa3daae508f48d7823d502816da80f2ec47500>`_)

`2.4.2 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.4.1...2.4.2>`_ (2020-11-01)
================================================================================================================

Bug Fixes
---------
* **gitlab_ci:** build images without releases (`06cb97e <https://gitlab.com/bbbatscale/bbbatscale/commit/06cb97e09746f762589f2d02b30535c23543dda0>`_)
* **support_chat:** fix users with supporter's group can not see the support overview (`a2c6ce0 <https://gitlab.com/bbbatscale/bbbatscale/commit/a2c6ce0d9c025b3561da1ee806046e176ba003eb>`_)

`2.4.3 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.4.2...2.4.3>`_ (2020-11-02)
================================================================================================================

Bug Fixes
---------
* **service:** meta webhook saving (`3f7e713 <https://gitlab.com/bbbatscale/bbbatscale/commit/3f7e71341f43762648365702e8e50538eef37977>`_)

`2.4.4 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.4.3...2.4.4>`_ (2020-11-02)
================================================================================================================

Bug Fixes
---------
* **service:** fix meta translation (`88f4557 <https://gitlab.com/bbbatscale/bbbatscale/commit/88f4557a34e9b3c6b2240d881d63bc3c1091972d>`_)

`2.4.5 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.4.4...2.4.5>`_ (2020-11-05)
================================================================================================================

Bug Fixes
---------
* **collect meetings:** default of room visibility set to false (`f1fdbbc <https://gitlab.com/bbbatscale/bbbatscale/commit/f1fdbbcc92f72baf3fb214b7711642b92daad90a>`_)
* **settings view:** include "jitsi enabled"-checkbox in settings view (`f3a8b5c <https://gitlab.com/bbbatscale/bbbatscale/commit/f3a8b5c93cc545189099fc0e8fa7e232cb11dc44>`_)

`2.5.0 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.4.5...2.5.0>`_ (2020-11-18)
================================================================================================================

Features
--------
* **groups:** add group management (`d34748f <https://gitlab.com/bbbatscale/bbbatscale/commit/d34748f687c39b06e139b0a5e9fae7f77d0892e9>`_)
* **groups:** add group management (`99f4a41 <https://gitlab.com/bbbatscale/bbbatscale/commit/99f4a41152ffc292242b1192f5411004f6ac5b68>`_)
* **occupancy:** possibility to disable occupancy columns on front page (`3aceb26 <https://gitlab.com/bbbatscale/bbbatscale/commit/3aceb261080882594d02303f1debbad78dbcfe9c>`_)
* **recordings:** Add mp4 Download from preconfigured share (`f641f85 <https://gitlab.com/bbbatscale/bbbatscale/commit/f641f851a43e5c77a0bb4adfba5b036998f1a37c>`_)
* **scheduling:** add new scheduling strategy RANDOM_PICK_FROM_LEAST_UTILIZED (`07f571d <https://gitlab.com/bbbatscale/bbbatscale/commit/07f571d8d2969feac9160a6bcbeab7d679cc88b3>`_)

Bug Fixes
---------
* **join:** Assigning Join-Privs for PersonalRooms (`9a98ff4 <https://gitlab.com/bbbatscale/bbbatscale/commit/9a98ff4a7d2c03df9ee6b1d7fb48a214ef03e9e3>`_)
* **lang cz:** Translation of CZ fixed (`4f8bd5e <https://gitlab.com/bbbatscale/bbbatscale/commit/4f8bd5eed6ccfe05bb9372ba38955592acefe779>`_)
* **occupancy:** possibility to disable occupancy columns on front page (`e36ac31 <https://gitlab.com/bbbatscale/bbbatscale/commit/e36ac31035f4922cc705ca945bfed6df24688d6c>`_)
* **oidc:** fix login redirects on joining a meeting when oidc is enabled (`54a1398 <https://gitlab.com/bbbatscale/bbbatscale/commit/54a1398288624b1103484353ad9537118d7794dc>`_)
* **settings view:** include "playback url"-field in settings view (`738c1cf <https://gitlab.com/bbbatscale/bbbatscale/commit/738c1cf269a2a64e499d42a515fbb52f53e6a66e>`_)

`2.6.0 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.5.0...2.6.0>`_ (2020-11-19)
================================================================================================================

Features
--------
* **support_chat:** add a badge to the support staff overview indicating that the user is a moderator (`a150fbc <https://gitlab.com/bbbatscale/bbbatscale/commit/a150fbc0e69a5dad92198242db9a5674dec6b222>`_)
* **user:** add user display name (`e797e82 <https://gitlab.com/bbbatscale/bbbatscale/commit/e797e82f390179bedd4be29766aa964ff6abdf99>`_)

Bug Fixes
---------
* **ui:** dark theme (`2a427cf <https://gitlab.com/bbbatscale/bbbatscale/commit/2a427cfdea0f29c4bee43f7df4fc43688b718bf1>`_)

`2.6.1 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.6.0...2.6.1>`_ (2020-11-20)
================================================================================================================

Bug Fixes
---------
* **room password:** fix wrong password when choosing ASK_MODERATOR in room startup (`9208d26 <https://gitlab.com/bbbatscale/bbbatscale/commit/9208d2694696b557008b5e7098ab1d123394fd3a>`_)

`2.6.2 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.6.1...2.6.2>`_ (2020-11-23)
================================================================================================================

Bug Fixes
---------
* **scheduling:** fix a bug where non worker maschines where choosen for room scheduling (`32f3024 <https://gitlab.com/bbbatscale/bbbatscale/commit/32f30241704f05efe18e1503df77d9fdf9aa810e>`_)
* **scheduling:** fix a bug where non worker maschines where choosen for room scheduling (`766bc6a <https://gitlab.com/bbbatscale/bbbatscale/commit/766bc6a55706743a01c3fc9f8a830c20fd316edc>`_)
* **testing:** change necessary tests (`ad2efa6 <https://gitlab.com/bbbatscale/bbbatscale/commit/ad2efa668344a11fbb765849c0a4ffbafd63c4c3>`_)

`2.7.0 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.6.2...2.7.0>`_ (2020-11-26)
================================================================================================================

Features
--------
* **helm_chart:** add kubernetes helm charts for openshift (`7f11f3e <https://gitlab.com/bbbatscale/bbbatscale/commit/7f11f3e33cd59aaacdce05686c3f51c597652ba8>`_)
* **join:** add login button to join meeting page (`82f399e <https://gitlab.com/bbbatscale/bbbatscale/commit/82f399e5dbfd47994cf87035556df61c7075fce6>`_)
* **support_chat:** add a view to display all supporters and their (in-)active states (`c6fdbbf <https://gitlab.com/bbbatscale/bbbatscale/commit/c6fdbbf51542aa66a4c7415d329d4b6e0fdc85d4>`_)
* **support_chat:** add the ability to add prepared answers (`543a962 <https://gitlab.com/bbbatscale/bbbatscale/commit/543a962581502212bfd5a2329221f52cd969d83f>`_)
* **support_chat:** add the ability to change the message maximum length (`135f694 <https://gitlab.com/bbbatscale/bbbatscale/commit/135f6941bb21125d2724bcc969c7e9b044c13607>`_)
* **support_chat:** add the ability to disable the chat if no supporter is online (`57c11f0 <https://gitlab.com/bbbatscale/bbbatscale/commit/57c11f0c5540c2e93e04bd9883db26f9a1808a79>`_)

Bug Fixes
---------
* **get join password:** corrected the return of moderator password for BBB session (`2699e09 <https://gitlab.com/bbbatscale/bbbatscale/commit/2699e09454804c1a763313b5575fedf54ae65bd4>`_)
* **helm_chart:** cronjob command directly executed as part of the CronJob (`2a4a80b <https://gitlab.com/bbbatscale/bbbatscale/commit/2a4a80b76c503d474d240b23cb4d970d106740f7>`_)
* **join:** fix incorrect login url (`191585b <https://gitlab.com/bbbatscale/bbbatscale/commit/191585ba7ea26e4abc8fa7e56fa85212a827d865>`_)
* **join:** use url and translation tags (`5e4c316 <https://gitlab.com/bbbatscale/bbbatscale/commit/5e4c316aa9dff4974922666465e28f0e7e65b761>`_)
* **view:** fix fail on missing headers HTTP_HOST and HTTP_AGENT (`3bff1f4 <https://gitlab.com/bbbatscale/bbbatscale/commit/3bff1f425270f0dae055ef8dbcedaf411c7f061d>`_)
* **view:** fix fail on missing headers HTTP_HOST and HTTP_AGENT - for real this time (`999ca62 <https://gitlab.com/bbbatscale/bbbatscale/commit/999ca623c5f7acfc60d2a9dd7fc4fd9ad49f3f51>`_)

Other
-----
* **collect_instance_stats:** deprecate collect_instances_stats command. replaced with collect_server_stats. (`fe41c0d <https://gitlab.com/bbbatscale/bbbatscale/commit/fe41c0d874704bfaa51806a5ac212683df487051>`_)
* **collect_server_stats:** refactor logging for collect_server_stats command (`3fb80f3 <https://gitlab.com/bbbatscale/bbbatscale/commit/3fb80f31ecfa391948f552d2b0661d19c1f1382a>`_)
* **event collection:** completely rework how the event collectors do logging. (`cb7c856 <https://gitlab.com/bbbatscale/bbbatscale/commit/cb7c856b4e1204203607a720effb90c0b9206f5e>`_)
* **event collection:** refactor logging for collect_all_room_occupancy command. (`f6662f5 <https://gitlab.com/bbbatscale/bbbatscale/commit/f6662f5e87b2b8e0d8df833ce8099d6673837e99>`_)
* **helm_chart:** change directory structure (`bbf9bfc <https://gitlab.com/bbbatscale/bbbatscale/commit/bbf9bfc7df0924b678f32c39fddc12317b5393c8>`_)
* **helm_chart:** change main variable name to camelCase (`277e21b <https://gitlab.com/bbbatscale/bbbatscale/commit/277e21b69213aa9d32573f5a3c2f4d581a8d6000>`_)
* **helm_chart:** remove dependency tarballs (`6862e98 <https://gitlab.com/bbbatscale/bbbatscale/commit/6862e981ece907fb13df3ee6bc0f2473106ef60b>`_)
* **house_keeping:** refactor logging for house_keeping command (`8a0e185 <https://gitlab.com/bbbatscale/bbbatscale/commit/8a0e185535843ea278a6a89cda1ec5fb0c0b49a6>`_)
* **readme:** rework project status (`29f282c <https://gitlab.com/bbbatscale/bbbatscale/commit/29f282c8a1b23238ebebca4dd7682042835ef535>`_)
* **services:** refactor logging for services (`e534664 <https://gitlab.com/bbbatscale/bbbatscale/commit/e5346649b3f1db8abce7dd605e03afbeac155174>`_)
* **setup.md:** Added turn stun setup (`09ca910 <https://gitlab.com/bbbatscale/bbbatscale/commit/09ca9105d88db65f2359d95ac975296f5139423e>`_)
* **signals:** refactor logging for signals (`650044a <https://gitlab.com/bbbatscale/bbbatscale/commit/650044acda7f7088ee6d4901ad1a377e023b1b26>`_)
* **translation cz:** add room translation (`310c79b <https://gitlab.com/bbbatscale/bbbatscale/commit/310c79bae65a9f5914661d7a865934970845e6a2>`_)
* **utils:** refactor logging for utils moduls (`ccd3247 <https://gitlab.com/bbbatscale/bbbatscale/commit/ccd3247f72f8a395b0c407397e957c5929dd65f9>`_)
* **utils:** refactor logging for utils moduls (`66e2753 <https://gitlab.com/bbbatscale/bbbatscale/commit/66e27536ac2c2a8b6fe77286e1cd0177f1277385>`_)
* **views:** refactor logging for all views (`16bde8f <https://gitlab.com/bbbatscale/bbbatscale/commit/16bde8fb1635cbf4da875b1b17f8675db0bd3fc0>`_)

`2.8.0 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.7.0...2.8.0>`_ (2020-12-06)
================================================================================================================

Features
--------
* **stream:** Added Streaming URL for a meeting (`5a9b231 <https://gitlab.com/bbbatscale/bbbatscale/commit/5a9b23177a309aa6567812c85897acf0a1540c3c>`_)

Bug Fixes
---------
* **html:** DownloadIcon Repair (`a65e655 <https://gitlab.com/bbbatscale/bbbatscale/commit/a65e655ba145895c020ea2847dc71882f46f5d84>`_)
* **streaming:** pytest fixup url (`c40473d <https://gitlab.com/bbbatscale/bbbatscale/commit/c40473d35dbac1cb795605c084f3bee7a2be92de>`_)
* **trans:** add field that we missed... (`7b1fd87 <https://gitlab.com/bbbatscale/bbbatscale/commit/7b1fd87b8efcfc214a53f642184b4c742da8378c>`_)

Other
-----
* **MoodleRoom:** refactored moodle room into external room, so every external room created over API's is considered the same (`eedfc13 <https://gitlab.com/bbbatscale/bbbatscale/commit/eedfc132985832e2c4190acf139bcc9980f9f4b2>`_)

`2.8.1 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.8.0...2.8.1>`_ (2020-12-07)
================================================================================================================

Bug Fixes
---------
* **rooms parameter:** streaming url parsing (`38cdc9b <https://gitlab.com/bbbatscale/bbbatscale/commit/38cdc9b23f6c829075cfaa810bcfa7f2969d4ed0>`_)

`2.8.2 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.8.1...2.8.2>`_ (2020-12-14)
================================================================================================================

Bug Fixes
---------
* **logging:** collect_stats (`1d0e5d6 <https://gitlab.com/bbbatscale/bbbatscale/commit/1d0e5d67b970da0a759baeeccfb8a12870ad8a39>`_)

`2.9.0 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.8.2...2.9.0>`_ (2021-01-04)
================================================================================================================

Features
--------
* **support_chat:** add 'inactivate all supporters' button (only for staff members) (`7e260bc <https://gitlab.com/bbbatscale/bbbatscale/commit/7e260bc75445aee90125eaeb5a1fe05ed70c0ab3>`_)

Bug Fixes
---------
* **trans:** update some fuzzy translations (`fbcafb3 <https://gitlab.com/bbbatscale/bbbatscale/commit/fbcafb3e8957a3824e176087bd0f2feab3405e2e>`_)

`2.9.1 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.9.0...2.9.1>`_ (2021-01-14)
================================================================================================================

Bug Fixes
---------
* don't accept 0 for participant_count_max or videostream_count_max (`9d9a9b1 <https://gitlab.com/bbbatscale/bbbatscale/commit/9d9a9b1ca3b433d7045daa14a0f28ff54f206e75>`_), closes `#121 <https://gitlab.com/bbbatscale/bbbatscale/issues/121>`_

Other
-----
* **DJANGO_SETTINGS_MODULE:** extract the default settings module into a variable (`cc86232 <https://gitlab.com/bbbatscale/bbbatscale/commit/cc86232c37e22416e427adf2280d0a140abd734f>`_)
* **docker:** rework Dockerfile, docker-entrypoint, and docker-compose (`decdf6e <https://gitlab.com/bbbatscale/bbbatscale/commit/decdf6e25fa023478da73a19e2ac8172e65a7b6e>`_)

`2.10.0 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.9.1...2.10.0>`_ (2021-01-14)
==================================================================================================================

Features
--------
* **shibboleth:** add shibboleth authentication to BBBatScale (`7518a9e <https://gitlab.com/bbbatscale/bbbatscale/commit/7518a9e1aa2e68fe9eaa1e467819718ccf922eb0>`_)

Bug Fixes
---------
* **models:** handle aggregate returning `None`:code: correctly and return 0 instead of `None`:code: (`999a628 <https://gitlab.com/bbbatscale/bbbatscale/commit/999a628dbb52efc41e00b001c8a11926c3c8242a>`_)

Other
-----
* **helm:** add helm templates to include nginx (`c69fc94 <https://gitlab.com/bbbatscale/bbbatscale/commit/c69fc941843a70214d6f4cc3fd4e64efbc94992c>`_)
* **helm:** add helm templates to include shibboleth-sp (`1d66631 <https://gitlab.com/bbbatscale/bbbatscale/commit/1d66631ece5fba0b4e8d70e12dcab562fa58fe7e>`_)
* **nginx:** improve nginx configurations and add http as well as https configurations (`03eb884 <https://gitlab.com/bbbatscale/bbbatscale/commit/03eb88442a0b885a6326c1a845abb6aa4d9b1c4a>`_)

`2.11.0 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.10.0...2.11.0>`_ (2021-01-18)
===================================================================================================================

Features
--------
* **enrollment-api:** add API support for server registration (`443c889 <https://gitlab.com/bbbatscale/bbbatscale/commit/443c889cb2269bf3f9284ac2428b7388bf3befcc>`_)
* **enrollment-api:** drop trailing slash from URL path (`065e6f8 <https://gitlab.com/bbbatscale/bbbatscale/commit/065e6f8ab4cf715c3f302841e6c298873df0e5ab>`_)
* **enrollment-api:** introduce machine_id field in Server model (`3640b75 <https://gitlab.com/bbbatscale/bbbatscale/commit/3640b75bc9f3ccbf8ce552de70189d99b3e4e551>`_)

Bug Fixes
---------
* **ui:** adjust support chat overview (`081932e <https://gitlab.com/bbbatscale/bbbatscale/commit/081932eff77a335bce40fcb9b1d5b5a2915482da>`_)
* **ui:** make sidebar and navbar sticky (`c57805a <https://gitlab.com/bbbatscale/bbbatscale/commit/c57805a8e699908c491896e279f3f0244e08fc70>`_), closes `#101 <https://gitlab.com/bbbatscale/bbbatscale/issues/101>`_

`2.11.1 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.11.0...2.11.1>`_ (2021-01-24)
===================================================================================================================

Bug Fixes
---------
* fix collect_server_stats (`d4c9188 <https://gitlab.com/bbbatscale/bbbatscale/commit/d4c9188cbd3fe641774d77da744b8f5230a2b0f2>`_)

`2.12.0 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.11.1...2.12.0>`_ (2021-02-12)
===================================================================================================================

Features
--------
* **create rooms:** option for moderator to wait for room to be opened (`ac053e5 <https://gitlab.com/bbbatscale/bbbatscale/commit/ac053e5d065771e4c3e06722125a120ae617b898>`_), closes `#98 <https://gitlab.com/bbbatscale/bbbatscale/issues/98>`_

Bug Fixes
---------
* **login_shibboleth:** fix an issue were the moderators and supporters group would lead to an exception (`e7f8601 <https://gitlab.com/bbbatscale/bbbatscale/commit/e7f86011b251403630c24ad570b194597c57dab3>`_)
* **login_shibboleth:** fix more_clear_input_headers instruction using underscores instead of hyphens (`ede6817 <https://gitlab.com/bbbatscale/bbbatscale/commit/ede68172bed8b1220a277031844348a578743481>`_)
* **login_shibboleth:** fix shibboleth headers are decoded with `ISO-8859-1`:code: instead of `UTF-8`:code: (`3eeb241 <https://gitlab.com/bbbatscale/bbbatscale/commit/3eeb2413116a2b4efdda88b8b2aff16f7d6d5a50>`_)
* **login_shibboleth:** fix stylesheet not correctly referenced and remove dfn metadata provider (`1b4a5ae <https://gitlab.com/bbbatscale/bbbatscale/commit/1b4a5ae37711425477f5a858c08129b20e69d290>`_)
* **ui:** create room (`15cb670 <https://gitlab.com/bbbatscale/bbbatscale/commit/15cb670acf96826a76d5b691b1dc740d6cfd13d5>`_)

Other
-----
* **login_shibboleth:** change the username retrieved from shibboleth from uid to subject-id (`f07cb07 <https://gitlab.com/bbbatscale/bbbatscale/commit/f07cb075d1b137360af5b202284c166729d9f36d>`_)
* **login_shibboleth:** shibboleth is now always involved not just in the login and logout phase (`bcdb0cf <https://gitlab.com/bbbatscale/bbbatscale/commit/bcdb0cf7ae1c2f7dc72521c42cab39c857ebc216>`_)
* **nginx:** add the ability to supply additional server configs (`02ab9b1 <https://gitlab.com/bbbatscale/bbbatscale/commit/02ab9b1c412b07a962261da3ad868ec1a1d4324d>`_)
* changed wording in !136 (`639d785 <https://gitlab.com/bbbatscale/bbbatscale/commit/639d7851fbb8e2db18ec3d0712f3fdcbf8157e65>`_)
* changed wording in !136 (`1cd8db1 <https://gitlab.com/bbbatscale/bbbatscale/commit/1cd8db18cd3ab884d24d5e569dd6726c7a4079a6>`_)

`2.13.0 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.12.0...2.13.0>`_ (2021-02-12)
===================================================================================================================

Features
--------
* **oidc:** enable authorization against satosa oidc proxy (`729a7d3 <https://gitlab.com/bbbatscale/bbbatscale/commit/729a7d3f40375bb7a642998b297dee105789fbda>`_), closes `#124 <https://gitlab.com/bbbatscale/bbbatscale/issues/124>`_

Other
-----
* **ldap:** allow usage of custom ldap backends (`821b263 <https://gitlab.com/bbbatscale/bbbatscale/commit/821b26314825aac1b9df9308fc3156ee9d0f42b5>`_)

`2.13.1 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.13.0...2.13.1>`_ (2021-02-13)
===================================================================================================================

Bug Fixes
---------
* **login_shibboleth:** fix username not interpreted case insensitively (`0f0f712 <https://gitlab.com/bbbatscale/bbbatscale/commit/0f0f71227792d68e9decaaf73d89c40a8645a181>`_)
* **user:** add missing migration (`54941da <https://gitlab.com/bbbatscale/bbbatscale/commit/54941dad4de181f717443fbd61a0eb0b082a7054>`_)

`2.13.2 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.13.1...2.13.2>`_ (2021-03-18)
===================================================================================================================

Bug Fixes
---------
* **ui:** replace static 'Search room ...' with a translatable text (`e689752 <https://gitlab.com/bbbatscale/bbbatscale/commit/e6897529d335defeaba1838a4aa241c5953bbef7>`_), closes `#133 <https://gitlab.com/bbbatscale/bbbatscale/issues/133>`_
* fixed initial create on fresh restarted server to fall into an 500 error even if meeting was created (`3e02455 <https://gitlab.com/bbbatscale/bbbatscale/commit/3e02455120a86ace6de8d8f637e5c2f0e16f6e8c>`_)
* triggers bootstrap tooltip init (`73b2056 <https://gitlab.com/bbbatscale/bbbatscale/commit/73b2056c4242f74431b681531e63d9b52103ce0d>`_)

Other
-----
* **login_shibboleth:** use pairwise-id instead of subject-id to prevent Cross-Site-Tracking (`d8d024c <https://gitlab.com/bbbatscale/bbbatscale/commit/d8d024c005324cbf259e702f1aed5b83f4e66f75>`_)

`2.13.3 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.13.2...2.13.3>`_ (2021-03-24)
===================================================================================================================

Bug Fixes
---------
* added missing email to local user form and overview tables (`a3edfa1 <https://gitlab.com/bbbatscale/bbbatscale/commit/a3edfa15b568700f3add8a02c0441fce810571c9>`_)
* logo max with for larger pictures (`b20e3ff <https://gitlab.com/bbbatscale/bbbatscale/commit/b20e3ff26d2c5751579ebfaa11d0aa998a74ed08>`_)

Other
-----
* **create_meeting:** move `Join waiting room`:code: up and add description (`2516818 <https://gitlab.com/bbbatscale/bbbatscale/commit/2516818cdfcf941fa276aedc2318f33b3d0fa036>`_), closes `#139 <https://gitlab.com/bbbatscale/bbbatscale/issues/139>`_
* **shibboleth-sp:** update shibboleth-sp to version 3.2.1 to mitigate a security issue (`e33ccba <https://gitlab.com/bbbatscale/bbbatscale/commit/e33ccbacba33090e3e7b4cd6d86ed95880b45dfa>`_)

`2.14.0 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.13.3...2.14.0>`_ (2021-03-31)
===================================================================================================================

Features
--------
* **user:** reset local user password as admin (`02f1427 <https://gitlab.com/bbbatscale/bbbatscale/commit/02f1427539f82eb20ac235a87f1594265df3d5c5>`_)

Bug Fixes
---------
* **homeroom:** remove auto deletion/creation of homerooms (`e4776d0 <https://gitlab.com/bbbatscale/bbbatscale/commit/e4776d053a0b91f0d8c2f9ac74858598a7d5578e>`_), closes `#84 <https://gitlab.com/bbbatscale/bbbatscale/issues/84>`_
* bug with only one entry in table overview (`d59863c <https://gitlab.com/bbbatscale/bbbatscale/commit/d59863cfbcd5d1ab45a8a04bd127eaa5c612ff1c>`_)
* logo width parameter to max-width for displaying small logos (`06d2d94 <https://gitlab.com/bbbatscale/bbbatscale/commit/06d2d945a610b0e535724268f277c6b068ee81f5>`_)

Other
-----
* **helm:** disable support chat by default (`88924e2 <https://gitlab.com/bbbatscale/bbbatscale/commit/88924e2b340945d1421c97fc6ba6dfd2663fa806>`_)
* **signals:** replace appropriate signals with `Field.default`:code: (`8666fd8 <https://gitlab.com/bbbatscale/bbbatscale/commit/8666fd807235de34ceb75349b4937bb7e1d02c34>`_)
* **translation:** regenerate translation files (`440c2eb <https://gitlab.com/bbbatscale/bbbatscale/commit/440c2eba7c9e6d04d87f76586b2bb2d43b3283a0>`_)
* reverted template changes in commit a3edfa15 for !148 (`a3b07b4 <https://gitlab.com/bbbatscale/bbbatscale/commit/a3b07b4947cf84629cbb30406d6801cc92c3ebe3>`_)

`2.15.0 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.14.0...2.15.0>`_ (2021-05-11)
===================================================================================================================

Features
--------
* extended moderation modes (`1d54fcd <https://gitlab.com/bbbatscale/bbbatscale/commit/1d54fcda6343a7c0fe8a9a0289bc7b982d1dd28b>`_), closes `#45 <https://gitlab.com/bbbatscale/bbbatscale/issues/45>`_

Bug Fixes
---------
* **table layout:** resolves rendered table issues caused by changes in !152 (`79c0899 <https://gitlab.com/bbbatscale/bbbatscale/commit/79c08995e8e7877bb76c8f1a2e72fe8c0344341f>`_)
* **ui:** in home.html consistently add tooltips to actionable icons (`66ac143 <https://gitlab.com/bbbatscale/bbbatscale/commit/66ac143b2275f8bc96a0bc6d45e78ee823e359df>`_)
* add timeout to database connection (`3f88d2c <https://gitlab.com/bbbatscale/bbbatscale/commit/3f88d2c5566e9346cefcc0f323790d9d604ee52c>`_)

Build System
------------
* **docker:** remove gratuitous docker-entrypoint.sh script (`d69c70c <https://gitlab.com/bbbatscale/bbbatscale/commit/d69c70c11d5f410ced96ab52558e59e7135ee1fc>`_)
* **requirements:** remove gunicorn and use uvicorn as a single worker in the docker image (`bf66642 <https://gitlab.com/bbbatscale/bbbatscale/commit/bf666427a0639fa2458d5c248120af05c09832f3>`_)
* **requirements:** replace requirements.txt with Pipfile (`f0cc2dc <https://gitlab.com/bbbatscale/bbbatscale/commit/f0cc2dc99cae2f5e8897133a7f3ac00e6e8134ce>`_)

Other
-----
* **access_code:** remove access_code_guests and add only_prompt_guests_for_access_code (`27d8259 <https://gitlab.com/bbbatscale/bbbatscale/commit/27d8259b9657c1d88b2bf1235550ea2932e489a6>`_)
* **import_export:** revise import/export and improve UI experience (`4580224 <https://gitlab.com/bbbatscale/bbbatscale/commit/45802248bde562653c0493bb3fedace8cb14dd8d>`_)
* **nginx:** add the ability to change the X-Forwarded-Proto header value (`15c83ad <https://gitlab.com/bbbatscale/bbbatscale/commit/15c83ad0179cd7154eb1a5b35b106cd6939cf173>`_)
* **room_config:** unify, reorder, and update set/update-room-config's (`3bd1593 <https://gitlab.com/bbbatscale/bbbatscale/commit/3bd1593485e552eb0ed31739b5b68caa6b73318e>`_)
* **ui:** fix 'czech' not being translated correctly (`f8979e5 <https://gitlab.com/bbbatscale/bbbatscale/commit/f8979e5b20caa88aae2b6fc157e75de923a53fd2>`_)
* **ui:** update favicon using https://realfavicongenerator.net/ (`8b6776d <https://gitlab.com/bbbatscale/bbbatscale/commit/8b6776dd605a06313c108f4e147beaa1cc1311f1>`_)
* **views:** condense if's and improve db queries (`b004d62 <https://gitlab.com/bbbatscale/bbbatscale/commit/b004d62d31b7eb7c6128f0a7d6f2b9d86fcaf2ca>`_)
* **views:** fix login redirects do not use `settings.LOGIN_URL`:code: and quote `next`:code: (`bd5889d <https://gitlab.com/bbbatscale/bbbatscale/commit/bd5889d706a31158bdc3e7885cf761bf1279642c>`_)
* **views:** move utility functions into api.py and replace XMLs written by hand with xmltodict (`2e6f09c <https://gitlab.com/bbbatscale/bbbatscale/commit/2e6f09cf44cd54439fe1dace42dcc2a2b44cb099>`_)
* **views:** refurbish api views (`3eb7f70 <https://gitlab.com/bbbatscale/bbbatscale/commit/3eb7f70a0dbaec7401134a83cd75fc9fb2b32bbc>`_)
* **views:** refurbish create/join views (`ca23902 <https://gitlab.com/bbbatscale/bbbatscale/commit/ca2390251ad903c10048b2ce3331e30c597ac698>`_)
* **views:** split up views into separate files (`71fdcd2 <https://gitlab.com/bbbatscale/bbbatscale/commit/71fdcd2d6f738f3c78827c944716ac42ef92855e>`_)
* add separate setting for testing with all feature toggles enabled (`dc15caa <https://gitlab.com/bbbatscale/bbbatscale/commit/dc15caa5c866e32172c5a0fdbe4a0c0a6ed85d9d>`_)
* remove (effectively) dead coded (`87a7ced <https://gitlab.com/bbbatscale/bbbatscale/commit/87a7ced37a067a589259033540919ea737551710>`_)

`3.0.0 <https://gitlab.com/bbbatscale/bbbatscale/compare/2.15.0...3.0.0>`_ (2021-06-06)
=======================================================================================

⚠ BREAKING CHANGES
------------------
* **build:** revert combining nginx and uvicorn with nginx unit Refs: fd7a0eb1, 37ab5373, 88f2c3f1
* **docker:** change BBB@Scale port to 8080
* **docker:** combine nginx and uvicorn with nginx unit
* **docker:** move BBB@Scale working directory to /bbbatscale
* **docker:** remove `run`:code: subcommand from bbbatscale command
* **oidc:** Removed previous oidc auth implementation
* Removed /building/room url since deprecated
* Removed collect_instances_stats since deprecated
* Removed previous oidc auth implementation
* Removed python/django ldap support. LDAP authentication is still possible via oidc/keycloak.

Features
--------
* **models:** adds BigAutoField as pk for models (`ade9ec3 <https://gitlab.com/bbbatscale/bbbatscale/commit/ade9ec3258b1d2b6a641286179616e33c6da0a5c>`_)
* **oidc:** adds tenant based authentication (`5a8ca0c <https://gitlab.com/bbbatscale/bbbatscale/commit/5a8ca0c63926715e0cbb00eeae28ebceba431c6b>`_)
* **oidc:** allow tenant to use oidc unique username standard or own unique username claim (`850d4ac <https://gitlab.com/bbbatscale/bbbatscale/commit/850d4ac4889a02d8fbbe5323659e6067fa77a9f2>`_)
* **ui:** tenant auth parameter management for superuser (`f1628f2 <https://gitlab.com/bbbatscale/bbbatscale/commit/f1628f221bca4b13e3d8b8d6df139ec402090ea2>`_)
* **ui:** tenant management for superuser (`2853bcd <https://gitlab.com/bbbatscale/bbbatscale/commit/2853bcd775d6e398c527f0670ae69d6a388e2e5e>`_)
* **views:** added `reset_stucked_room`:code: cli method to frontend rooms overview (`3dc2987 <https://gitlab.com/bbbatscale/bbbatscale/commit/3dc2987c7c7ca2417ba20ea62c6e05bb1c02f3fa>`_)
* **views:** added copy option for secret values inside tables (`a41f115 <https://gitlab.com/bbbatscale/bbbatscale/commit/a41f115c3b7c2f57bf951dab86ad9112f4c08099>`_)
* **views:** added registration token in overview and form of scheduling strategy (`af5f64b <https://gitlab.com/bbbatscale/bbbatscale/commit/af5f64bdb0d1548e60765bfc43a071f4ef6f618c>`_)
* **views:** changed server overview and creation form to match django admin (`f8930e0 <https://gitlab.com/bbbatscale/bbbatscale/commit/f8930e0d3261cf7453af7bcfba3db5b0cb686757>`_)
* **views:** changed server overview and creation form to match django admin (`df30314 <https://gitlab.com/bbbatscale/bbbatscale/commit/df303147b1719973b203f7be1e869c7cf17f468a>`_)
* **views:** server side pagination for home view for performance improvment (`7cdea1a <https://gitlab.com/bbbatscale/bbbatscale/commit/7cdea1a07c413ed1618c80271953e445547f5511>`_)
* **views:** show creator email of recording in recordings overview if available (`45a61d3 <https://gitlab.com/bbbatscale/bbbatscale/commit/45a61d337f79ee4ab5215aa5852ad2409c651f4a>`_)
* adds tenant behavior to User, GeneralParameter, SchedulingStrategy, Server, Room (`4e0d07d <https://gitlab.com/bbbatscale/bbbatscale/commit/4e0d07d4d9e58756f6993b8212d6b9f4d2cbd3ae>`_)
* enable or disable recordings for each individual tenant (`f36f6cc <https://gitlab.com/bbbatscale/bbbatscale/commit/f36f6ccc175147298bae8b67f01579d951fefbc7>`_)

Bug Fixes
---------
* **commands:** fixed django initial check when sites not migrated yet (`dfbf7c5 <https://gitlab.com/bbbatscale/bbbatscale/commit/dfbf7c5e513da6986947b6380d691cff456b7d84>`_)
* **import_export:** fix deserialize does not add tenants (`4343e8f <https://gitlab.com/bbbatscale/bbbatscale/commit/4343e8f84a437ec29fb69aadb39e52ffe3d538be>`_)
* **models:** added comment for default tenant fix (`5515a0a <https://gitlab.com/bbbatscale/bbbatscale/commit/5515a0ab0acbccb490ed08d011b2b4872e95da97>`_)
* **models:** fixed get_default_tenent (`46ec0bd <https://gitlab.com/bbbatscale/bbbatscale/commit/46ec0bdda6179ed012307796d8dc54049c9407bc>`_)
* **oidc:** add auth parameter signals to update site cache (`c8bdb33 <https://gitlab.com/bbbatscale/bbbatscale/commit/c8bdb337db3f7e1f7283d7e2175d3ff21a7e71d0>`_)
* **oidc:** fix oidc authentication failing instead of returning `None`:code: (`2366410 <https://gitlab.com/bbbatscale/bbbatscale/commit/23664103a73856aad167454dbeccc3919d12d6da>`_)
* **oidc:** remove unnecessary email, wrong comment and documentation (`d16631b <https://gitlab.com/bbbatscale/bbbatscale/commit/d16631b4fedd69d0b8ff2b913dc66785f480a680>`_)
* **support_chat:** enable support chat url to use slashes in route for username (`ec389f0 <https://gitlab.com/bbbatscale/bbbatscale/commit/ec389f089bb0d04a58c483046588792374cf71cf>`_)
* **views:** fix wrong usage of `reverse`:code: (`0eccc68 <https://gitlab.com/bbbatscale/bbbatscale/commit/0eccc68db56b096bb43ea7f70783b980e4e70ff0>`_)
* **views:** ignore old datatable local storage (`708a952 <https://gitlab.com/bbbatscale/bbbatscale/commit/708a95282fa8876c000552237f96c4ad28b3f223>`_)
* **views:** set current tenant for newly created user (`e5e729d <https://gitlab.com/bbbatscale/bbbatscale/commit/e5e729d66247e904b66da698b95852ee11fc65f1>`_)
* **views:** statistics showing tenant specific data (`75a2761 <https://gitlab.com/bbbatscale/bbbatscale/commit/75a27611ef9eb6c11665cefc03b668bc7a83cc9f>`_)
* added missing translation of constants (`084a7bc <https://gitlab.com/bbbatscale/bbbatscale/commit/084a7bc87a9b77e74ef03af604a7f5d94f7f2f24>`_)
* adds tenant functionality to theme and recordings (`04eff3b <https://gitlab.com/bbbatscale/bbbatscale/commit/04eff3bc109cf2ebc530c9ccd18e46b52ee254a1>`_)
* applying full room config in function apply_room_config (`99c0c8e <https://gitlab.com/bbbatscale/bbbatscale/commit/99c0c8e361cd8574c34f948ce8fa2a25fcb4db5c>`_)
* black, isort and flake8 issues (`3dd83d7 <https://gitlab.com/bbbatscale/bbbatscale/commit/3dd83d7798017d36e8e907e803dae05f73bda6d8>`_)
* error with external bbb api (`eab1568 <https://gitlab.com/bbbatscale/bbbatscale/commit/eab15686b25f431c908017d039a399361306e0db>`_)
* fix django complaining about overlapping static files directory (`9cdaea7 <https://gitlab.com/bbbatscale/bbbatscale/commit/9cdaea7a7ab62411ced939454a9ec680c9f596a4>`_)
* fix static files copied to the wrong directory (`72b40b4 <https://gitlab.com/bbbatscale/bbbatscale/commit/72b40b4bfb592dae88eb008071ece80e77892798>`_)
* get_join_password (`272933f <https://gitlab.com/bbbatscale/bbbatscale/commit/272933faa0b3b54fa757527e870a0e28868fd26a>`_)

Reverts
-------
* **build:** combining nginx and uvicorn with nginx unit (`c24b200 <https://gitlab.com/bbbatscale/bbbatscale/commit/c24b2005a2fde7fe350ce3a41879e4a1a64e98ac>`_)

Translations
------------
* **german:** added missing translations and removed wrong fuzzy translations (`354833c <https://gitlab.com/bbbatscale/bbbatscale/commit/354833c72d55de54e2584c2e4f3cf26c483f3ae3>`_)

Build System
------------
* **docker:** change BBB@Scale port to 8080 (`88f2c3f <https://gitlab.com/bbbatscale/bbbatscale/commit/88f2c3f18ae6652365c3672996cb31b47d60a3fa>`_)
* **docker:** combine nginx and uvicorn with nginx unit (`fd7a0eb <https://gitlab.com/bbbatscale/bbbatscale/commit/fd7a0eb105f55a2b54febd95784d27e0e3b13ec9>`_)
* **docker:** fix bbbatscale docker image not working without root user (`570eec0 <https://gitlab.com/bbbatscale/bbbatscale/commit/570eec0495c2db36b140a9e890495e2058ce884f>`_)
* **docker:** move BBB@Scale working directory to /bbbatscale (`1b5c27a <https://gitlab.com/bbbatscale/bbbatscale/commit/1b5c27a0ab651c779a92ffdbc3e41524eaf8621c>`_)
* **docker:** remove `run`:code: subcommand from bbbatscale command (`37ab537 <https://gitlab.com/bbbatscale/bbbatscale/commit/37ab5373f12fccb5efc053b9ece856f502dacd01>`_)
* **docker:** specify `asyncio`:code: as event loop for uvicorn since django does not support `uvloop`:code: (`ed52cf4 <https://gitlab.com/bbbatscale/bbbatscale/commit/ed52cf46e1f166acc563a10d0897ee5fc9c0b923>`_)

Other
-----
* **commands:** extracted reset stucked rooms into own command (`1c32c24 <https://gitlab.com/bbbatscale/bbbatscale/commit/1c32c241231ee7eed0b25e1b848b5f4494fefc63>`_)
* **commands:** extracted reset stucked rooms into own command (`9f43179 <https://gitlab.com/bbbatscale/bbbatscale/commit/9f43179042a2caa2be7cecf3c2e71cb5f9ffd389>`_)
* **commands:** removed collection of statistics inside of collect_server_stats.py (`1c48cbf <https://gitlab.com/bbbatscale/bbbatscale/commit/1c48cbf8ef19bca4e2bd6009d8cbbf2408777d8f>`_)
* **import_export:** added tenant export and import (`5f383c4 <https://gitlab.com/bbbatscale/bbbatscale/commit/5f383c48266ce94f9314b421a5cd6934c7808dda>`_)
* **models:** changed str method of models (`ec523a2 <https://gitlab.com/bbbatscale/bbbatscale/commit/ec523a24dbe7ac618d906ace740202d0d37bd4f2>`_)
* **models:** increased random token length (`4b98618 <https://gitlab.com/bbbatscale/bbbatscale/commit/4b98618efbc6da5ed82dc038cd7674bd9497f778>`_)
* **models:** rename tenant to scheduling strategy (`2eb2bd7 <https://gitlab.com/bbbatscale/bbbatscale/commit/2eb2bd7a5e09cb978ffa8f16bd2bbeb6da51f54d>`_)
* **oidc:** changed group mapping in claims (`33b5936 <https://gitlab.com/bbbatscale/bbbatscale/commit/33b593687305d31bd692afda37eb77438a316e2a>`_)
* **oidc:** improved oidc_authentication code quality (`bb557c5 <https://gitlab.com/bbbatscale/bbbatscale/commit/bb557c59fcd25f806d87e0161146becca47159a0>`_)
* **oidc:** renamed env variables for oidc (`b5850b6 <https://gitlab.com/bbbatscale/bbbatscale/commit/b5850b6f838bc1a5cb3e876937cdb3eb6e8e953d>`_)
* **oidc:** unique username with sub + iss (`4a4d80b <https://gitlab.com/bbbatscale/bbbatscale/commit/4a4d80b0838ee5b61b300722921d47c0a66a8bef>`_)
* **ui:** replace DataTable with vanilla JavaScript on the home page (`3c7ba89 <https://gitlab.com/bbbatscale/bbbatscale/commit/3c7ba89b4c8bab279fec4dccf60c1f86429259a6>`_)
* **views:** improved home view performance (`9da55b4 <https://gitlab.com/bbbatscale/bbbatscale/commit/9da55b47ad8dc718b6f89912f7b044480c226bac>`_)
* add temporary lifespan support until django channels support it (`1f881a0 <https://gitlab.com/bbbatscale/bbbatscale/commit/1f881a0ecb427d54c693eb4860a464c30fe81cec>`_)
* added tenant support to core functionalities (`7aca6e6 <https://gitlab.com/bbbatscale/bbbatscale/commit/7aca6e631306dc8efb00047bca563fef8b355382>`_)
* added tenant support to support chat (`ebb2606 <https://gitlab.com/bbbatscale/bbbatscale/commit/ebb26064c8c57431bdc496917371aadd8afc0d7c>`_)
* changed separate login and logout urls to unified urls (`673e021 <https://gitlab.com/bbbatscale/bbbatscale/commit/673e021a75764095e74172f1b16f04ddaa7707b1>`_)
* migrated supportchat migrations (`ffa86fc <https://gitlab.com/bbbatscale/bbbatscale/commit/ffa86fccc7cde491c8e784c3ef7ab010ce053ca7>`_)
* remove gratuitous `BASE_URL`:code: (`1f955cb <https://gitlab.com/bbbatscale/bbbatscale/commit/1f955cb71bbf9a4d23bbcccb4bd02a8b469a33b4>`_)
* removed collect_instances_stats command (`6e0c823 <https://gitlab.com/bbbatscale/bbbatscale/commit/6e0c8236111b54024d3298a63eee2399ba08c620>`_)
* removed deprecated URL and corresponding view (`d736d4a <https://gitlab.com/bbbatscale/bbbatscale/commit/d736d4a43bb745249ce39347d29ae0d79eb5b2a2>`_)
* removed indent (`3a07236 <https://gitlab.com/bbbatscale/bbbatscale/commit/3a07236c379483788b38c502d1d8871a5b033716>`_)
* removed python/django ldap support (`0e494cb <https://gitlab.com/bbbatscale/bbbatscale/commit/0e494cbd2c525e430c5618ab999100cb2c05029a>`_)
* removed shibboleth auth backend (`a968740 <https://gitlab.com/bbbatscale/bbbatscale/commit/a968740b8416435b51c1e35528eeb97d81e4a79a>`_)
* removed unnecessary tenant declaration inside load function (`dc94e9a <https://gitlab.com/bbbatscale/bbbatscale/commit/dc94e9a08ab255fe7ffc31c603ca0970bd25d7a2>`_)
* removed unused middleware (`5ecb15b <https://gitlab.com/bbbatscale/bbbatscale/commit/5ecb15beb2f4eeb5507a9a8be0118e665277b28f>`_)
* simplified check tenant functions and removed nullable tenant for load functions (`8320e97 <https://gitlab.com/bbbatscale/bbbatscale/commit/8320e97793137f96e0ec9422a9c9a0450a9054c9>`_)

`3.0.1 <https://gitlab.com/bbbatscale/bbbatscale/compare/3.0.0...3.0.1>`_ (2021-06-10)
======================================================================================

Bug Fixes
---------
* **ui:** fix home does not display any rooms if the support chat is disabled (`9a2012b <https://gitlab.com/bbbatscale/bbbatscale/commit/9a2012b1b446f425f13fe41ac6d696b7e8b93bf5>`_)
* fix `moderation_mode`:code: migration mapping wrong value (`748e3f7 <https://gitlab.com/bbbatscale/bbbatscale/commit/748e3f7d768e40d24954fd6c308e297da12ebacd>`_)

`3.1.0 <https://gitlab.com/bbbatscale/bbbatscale/compare/3.0.1...3.1.0>`_ (2021-07-06)
======================================================================================

Features
--------
* **commands:** add ability to create admin user with password via the fake data command (`e898ae3 <https://gitlab.com/bbbatscale/bbbatscale/commit/e898ae3536d11b09a4d027e3fdaa0ed7f5d0f4f8>`_)
* **commands:** add command to add fake data (`e62b92d <https://gitlab.com/bbbatscale/bbbatscale/commit/e62b92d16ae9edbfab0df60550f6a993d3a319cf>`_)
* **ui:** add the ability to change BBB specific join parameters for users (`0d7cc65 <https://gitlab.com/bbbatscale/bbbatscale/commit/0d7cc65b5f1e70df5ea2834e57c81f10d2d1b947>`_)
* **views:** save room without applying room config for room update (`9ffc1e6 <https://gitlab.com/bbbatscale/bbbatscale/commit/9ffc1e6fd900f715d209d83fe5e9e414645d9e89>`_)

Bug Fixes
---------
* **commands:** fix unusable `createsuperuser`:code: command and at it to the `bbbatscale`:code: command (`6f1e634 <https://gitlab.com/bbbatscale/bbbatscale/commit/6f1e6344947315154a56f45d4e528412f0db2464>`_)
* **ui:** fix broken radio buttons and replace with select (`c78fdef <https://gitlab.com/bbbatscale/bbbatscale/commit/c78fdefd07726b3209469dfe5171a8abd7179ecc>`_)
* **views:** bug after saving personal room edit form as co_owner, co_owner lost permissions (`e8118bf <https://gitlab.com/bbbatscale/bbbatscale/commit/e8118bf965ae3eb28e91671d450eddff22140817>`_)
* **webhooks:** sending logoutUrl as meta parameter instead of not used url parameter as intended (`56780c9 <https://gitlab.com/bbbatscale/bbbatscale/commit/56780c9815b6fc043248139f28efb150a022bee2>`_)

Build System
------------
* **requirements:** upgrade dependencies and remove `default_app_config`:code: (`3c0c24a <https://gitlab.com/bbbatscale/bbbatscale/commit/3c0c24ae18ed6d78d653d66f58182c5b87628c78>`_)

Other
-----
* **commands:** use arg parser of subcommands in `bbbatscale`:code: to be able to use defined options (`975a276 <https://gitlab.com/bbbatscale/bbbatscale/commit/975a276ae45c7fd71e56b869f1aba9269e432af2>`_)
* **models:** remove the ability to select an empty `guest_policy`:code: (`4549c3c <https://gitlab.com/bbbatscale/bbbatscale/commit/4549c3c60bbfb277ff9366aa3a4edcea59a05e69>`_)
* **ui:** add a popover to visualize and give a hint to the new user settings (`fbcb768 <https://gitlab.com/bbbatscale/bbbatscale/commit/fbcb768771825c50f004e9b23351cbb2ef9292d0>`_)
* **ui:** change `Enable Guestlobby`:code: to `Guest policy`:code: since it is not a boolean (`1851328 <https://gitlab.com/bbbatscale/bbbatscale/commit/1851328869c3ca48dd0a220b3c0eff8251b98fe0>`_)
* **ui:** remove login button at view set_username and fix global login button to redirect (`eb2e2b8 <https://gitlab.com/bbbatscale/bbbatscale/commit/eb2e2b81126aebb4421e2ad849562d31c9df962a>`_)
* enforce `listen_only_mode=false`:code: to ensure `auto_join_audio=true`:code: works (`b6eda10 <https://gitlab.com/bbbatscale/bbbatscale/commit/b6eda109a099bc2da0f865eff010ddd6e1871852>`_)
* make `listen_only_mode`:code: editable to be able to join listen only as well (`039344f <https://gitlab.com/bbbatscale/bbbatscale/commit/039344f85ac67d6fdc9393e970398be1fdd9d77d>`_)
* update `.po`:code: files (`24a84ab <https://gitlab.com/bbbatscale/bbbatscale/commit/24a84abdc722f97aad8454cfdc1a1d931056e980>`_)

Translations
------------
* **german:** add missing translations (`2d4ee4c <https://gitlab.com/bbbatscale/bbbatscale/commit/2d4ee4c7bb80f9b638713b53ac40f149441c50f4>`_)
* **german:** translate BBB specific join parameters and their descriptions (`5b4c9a4 <https://gitlab.com/bbbatscale/bbbatscale/commit/5b4c9a4b783703e38ee298f19b49478062da62b2>`_)
* **german:** translate listen only mode texts and guest policy (`d77e645 <https://gitlab.com/bbbatscale/bbbatscale/commit/d77e645cfc9d17d2ebbcc89faa2ec6cc48e498ac>`_)
* **german:** translate missing 3.0.0 messages/texts (`b468590 <https://gitlab.com/bbbatscale/bbbatscale/commit/b4685905bf3390dc27abed7962172ff5346a8bde>`_)

`3.2.0 <https://gitlab.com/bbbatscale/bbbatscale/compare/3.1.0...3.2.0>`_ (2021-08-24)
======================================================================================

Features
--------
* **api:** extends internal and exposed bbb api and adds post possibility (`1183cb3 <https://gitlab.com/bbbatscale/bbbatscale/commit/1183cb3f6862e5f61a3c29e9dfc35cc4572acd79>`_)
* **recordings:** prepares scale for communication with scale agent for recordings orchestration (`8daf74e <https://gitlab.com/bbbatscale/bbbatscale/commit/8daf74ef68fb1cdcab663baca3dd406a0eab7bd4>`_)
* **support_chat:** add the ability to enable the support chat on a per-tenant base (`b909fd7 <https://gitlab.com/bbbatscale/bbbatscale/commit/b909fd726bd2c8adf3a3b1ef32d3a119c9c48dab>`_)
* **ui:** add the ability to order the rooms by their participant count (`1306efb <https://gitlab.com/bbbatscale/bbbatscale/commit/1306efbf3f02d2bf2db0aad9c8fbee27ff7bf76b>`_)

Translations
------------
* **german:** update support chat translations (`7fca660 <https://gitlab.com/bbbatscale/bbbatscale/commit/7fca660444eb88ecd401aaeefd89ef5b6d8073d8>`_)

Other
-----
* **oidc:** remove obsolete oidc and oidc-refresh feature toggles (`fa6408b <https://gitlab.com/bbbatscale/bbbatscale/commit/fa6408bbcaf1b8ae3fb116348390cb27b1511538>`_)
* **ui:** visually hide the support chat settings if it has been disabled (`c99e2d4 <https://gitlab.com/bbbatscale/bbbatscale/commit/c99e2d4ec08da1bb646374be403215f63f10f438>`_)

`3.3.0 <https://gitlab.com/bbbatscale/bbbatscale/compare/3.2.0...3.3.0>`_ (2021-09-24)
======================================================================================

Features
--------
* **ui:** add the ability to directly join a room or gain creator/moderator privileges via an url (`57c30a6 <https://gitlab.com/bbbatscale/bbbatscale/commit/57c30a6c7481a5fdf49338ca25d568caaab1d144>`_)
* **ui:** option to hide statistics for anonymous users (`902b73b <https://gitlab.com/bbbatscale/bbbatscale/commit/902b73b87954ceecda2e5fe8887e3c72cb7e8444>`_), closes `#150 <https://gitlab.com/bbbatscale/bbbatscale/issues/150>`_
* **ui:** tenant specific config for hiding room tables, statistic page visibility and app title (`ea11183 <https://gitlab.com/bbbatscale/bbbatscale/commit/ea11183b0004a4b964b304c9f5ea15c2f40b0e2a>`_)

Bug Fixes
---------
* **oidc:** fix that users logged in via oidc see a change-password-prompt (`13f21b0 <https://gitlab.com/bbbatscale/bbbatscale/commit/13f21b059b299113a30c3112c9e40dfec21e7dbc>`_)
* **ui:** fix non-working join url copy button on the home page (`06e15b4 <https://gitlab.com/bbbatscale/bbbatscale/commit/06e15b401ce25af6be128bdb846d5b916f4768c3>`_)
* 0030 migration set unusable password (`b43ed91 <https://gitlab.com/bbbatscale/bbbatscale/commit/b43ed91d41fa971578dfdf7e3fe20e46a26a6d16>`_)

Other
-----
* **ui:** remove download button for recordings and improve ui usability (`107c548 <https://gitlab.com/bbbatscale/bbbatscale/commit/107c548393d4cfa30acb9eebd41fbdd01921e031>`_)
* **views:** fix `user_update`:code: view which was unable to save, since the password was missing (`d1e8e3a <https://gitlab.com/bbbatscale/bbbatscale/commit/d1e8e3af688247cbe7fd1fb9a0961cde7b4035d9>`_)

Translations
------------
* **german:** add translations for the clipboard copy toast (`4be7f84 <https://gitlab.com/bbbatscale/bbbatscale/commit/4be7f84d60bf0accec8b04985f197c47db2ade46>`_)
* **german:** add translations for the direct join (`f23b360 <https://gitlab.com/bbbatscale/bbbatscale/commit/f23b360d33d8d9c7f89461d3fd4701654c8d971b>`_)
* **german:** fix user create/update/delete translations (`9b8e0e3 <https://gitlab.com/bbbatscale/bbbatscale/commit/9b8e0e395f4a3831648b5b57b23c6c088363c33c>`_)
* **german:** gender, grammar, use passive language and some simplifications (`296fe4f <https://gitlab.com/bbbatscale/bbbatscale/commit/296fe4fa0cb1c418b6e85ce191806fcfc147ccb5>`_)

`3.3.1 <https://gitlab.com/bbbatscale/bbbatscale/compare/3.3.0...3.3.1>`_ (2021-10-07)
======================================================================================

Bug Fixes
---------
* **views:** fix non correctly working joining with the join-secret as an anonymous user (`6acb8e8 <https://gitlab.com/bbbatscale/bbbatscale/commit/6acb8e820026e2de482b187acdd913578b02a803>`_)

Other
-----
* **ui:** add a tooltip to the copy join url button (`5d80551 <https://gitlab.com/bbbatscale/bbbatscale/commit/5d8055146159e4f2e72e1f88ccf353eaa7bd144d>`_)

Translations
------------
* **german:** fix typos in copy join url menu translations (`4bb61c2 <https://gitlab.com/bbbatscale/bbbatscale/commit/4bb61c2418f9019cd33f3d9bae979f7059963468>`_)
