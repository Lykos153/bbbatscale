{{/* BBB@Scale environment variables */}}
{{- define "bbbatscale.env" -}}
{{ if (index .Values "postgresql-extern" "enabled") -}}
{{ with (index .Values "postgresql-extern") -}}
- name: POSTGRES_DB
  {{ with .database -}}
  {{ if kindIs "string" . -}}
  value: {{ . | quote }}
  {{- else -}}
  valueFrom:
    secretKeyRef:
      name: {{ .secretName | quote }}
      key: {{ .secretKey | quote }}
  {{- end }}
  {{- end }}
- name: POSTGRES_USER
  {{ with .username -}}
  {{ if kindIs "string" . -}}
  value: {{ . | quote }}
  {{- else -}}
  valueFrom:
    secretKeyRef:
      name: {{ .secretName | quote }}
      key: {{ .secretKey | quote }}
  {{- end }}
  {{- end }}
- name: POSTGRES_PASSWORD
  valueFrom:
    secretKeyRef:
      {{ with .password -}}
      {{ if kindIs "string" . -}}
      name: {{ $.Release.Name }}-postgresql-extern
      key: postgresql-password
      {{- else -}}
      name: {{ .secretName | quote }}
      key: {{ .secretKey | quote }}
      {{- end }}
      {{- end }}
- name: POSTGRES_HOST
  {{ with .host -}}
  {{ if kindIs "string" . -}}
  value: {{ . | quote }}
  {{- else -}}
  valueFrom:
    secretKeyRef:
      name: {{ .secretName | quote }}
      key: {{ .secretKey | quote }}
  {{- end }}
  {{- end }}
- name: POSTGRES_PORT
  {{ with .port | default 5432 -}}
  {{ if or (kindIs "int" .) (kindIs "string" .) -}}
  value: {{ . | quote }}
  {{- else -}}
  valueFrom:
    secretKeyRef:
      name: {{ .secretName | quote }}
      key: {{ .secretKey | quote }}
  {{- end }}
  {{- end }}
{{- end }}
{{- else -}}
- name: POSTGRES_DB
  value: {{ if .Values.postgresql.enabled -}}
    {{ .Values.postgresql.postgresqlDatabase | quote }}
  {{- else -}}
    {{ (index .Values "postgresql-ha" "postgresql" "database") | quote }}
  {{- end }}
- name: POSTGRES_USER
  value: {{ if .Values.postgresql.enabled -}}
    {{ .Values.postgresql.postgresqlUsername | quote }}
  {{- else -}}
    {{ (index .Values "postgresql-ha" "postgresql" "username") | quote }}
  {{- end }}
- name: POSTGRES_PASSWORD
  valueFrom:
    secretKeyRef:
      name: {{ .Release.Name }}-postgresql{{ if not .Values.postgresql.enabled }}-ha-postgresql{{ end }}
      key: postgresql-password
- name: POSTGRES_HOST
  value: {{ .Release.Name }}-postgresql{{ if not .Values.postgresql.enabled }}-ha-pgpool{{ end }}
- name: POSTGRES_PORT
  value: {{ if .Values.postgresql.enabled -}}
    {{ .Values.postgresql.service.port | quote }}
  {{- else -}}
    {{ (index .Values "postgresql-ha" "service" "port") | quote }}
  {{- end }}
{{- end }}
- name: REDIS_HOST
  value: {{ .Release.Name }}-redis-master
- name: REDIS_PORT
  value: {{ .Values.redis.master.service.port | quote }}
- name: REDIS_PASSWORD
  valueFrom:
    secretKeyRef:
      name: {{ .Release.Name }}-redis
      key: redis-password
- name: DJANGO_SECRET_KEY
  valueFrom:
    secretKeyRef:
      name: bbbatscale-secret
      key: djangoSecretKey
- name: RECORDINGS_SECRET
  valueFrom:
    secretKeyRef:
      name: bbbatscale-secret
      key: recordingsSecret
- name: DJANGO_SETTINGS_MODULE
  value: {{ .Values.bbbatscale.djangoSettingsModule }}
- name: SECURE_PROXY_SSL_HEADER
  value: HTTP_X_FORWARDED_PROTO
- name: BBBATSCALE_MODERATORS_GROUP
  value: {{ .Values.bbbatscale.moderatorsGroupName | quote }}
- name: BBBATSCALE_SUPPORTERS_GROUP
  value: {{ .Values.bbbatscale.supportersGroupName | quote }}
{{ if .Values.bbbatscale.supportChat.enabled -}}
- name: SUPPORT_CHAT
  value: enabled
{{- end }}
{{ if .Values.bbbatscale.webhooks.enabled -}}
- name: WEBHOOKS
  value: enabled
{{- end }}
{{- end }}

{{/* BBB@Scale config volumes */}}
{{- define "bbbatscale.configVolumes" -}}
- name: bbbatscale-config
  configMap:
    name: bbbatscale-configmap
    defaultMode: 0444
{{- end }}

{{/* BBB@Scale config volume mounts */}}
{{- define "bbbatscale.configVolumeMounts" -}}
{{ if .Values.bbbatscale.logging.enabled -}}
- name: bbbatscale-config
  readOnly: true
  mountPath: /bbbatscale/BBBatScale/settings/logging_config.py
  subPath: loggingConfig
{{- end }}
{{- end }}

{{/* Common Labels */}}
{{- define "commonLabels" -}}
app.kubernetes.io/managed-by: {{ .Release.Service }}
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/version: {{ .Chart.AppVersion }}
helm.sh/chart: {{ .Chart.Name }}-{{ .Chart.Version | replace "+" "_" }}
helm.sh/revision: {{ .Release.Revision | quote }}
{{- end }}
