import binascii
import hmac
import json
import logging
from datetime import datetime

import xmltodict
from core.constants import ROOM_STATE_ACTIVE, ROOM_STATE_CREATING, ROOM_STATE_INACTIVE, SERVER_STATE_UP
from core.models import ExternalRoom, GeneralParameter, Meeting, Room, SchedulingStrategy, Server, ServerType
from core.services import (
    delete_recordings,
    external_get_or_create_room_with_params,
    meeting_create,
    parse_dict_to_xml,
    parse_recordings_from_multiple_sources,
    publish_recordings,
    reset_room,
    update_recordings,
)
from core.utils import BigBlueButton, validate_bbb_api_call
from core.views import create_view_access_logging_message
from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseNotAllowed, JsonResponse
from django.shortcuts import redirect
from django.utils.timezone import now
from django.views.decorators.csrf import csrf_exempt

logger = logging.getLogger(__name__)


@csrf_exempt
def callback_bbb(request):
    logger.info(create_view_access_logging_message(request))

    logger.info("start processing bbb callback request " + request.POST["event"])

    token = request.META["HTTP_AUTHORIZATION"].split(" ")[1]
    event = json.loads(request.POST["event"])[0]["data"]
    domain = request.POST["domain"]
    event_id = event["id"]
    server = Server.objects.get(dns=domain)
    if hmac.compare_digest(token.encode("UTF-8"), server.shared_secret.encode("UTF-8")):
        logger.debug("token validation passed.")

        if event_id in ["meeting-created", "meeting-ended"]:
            meeting_id = event["attributes"]["meeting"]["external-meeting-id"]
            logger.debug("event %s for meeting (id=%s, domain=%s)" % (event_id, meeting_id, domain))
            if event_id == "meeting-created":
                breakout = event["attributes"]["meeting"]["is-breakout"]
                name = event["attributes"]["meeting"]["name"]
                meta = translate_bbb_meta_data(event["attributes"]["meeting"]["metadata"])
                logger.debug("meeting created (domain=%s, id=%s, name=%s)" % (domain, meeting_id, name))
                meta.update(
                    name=name,
                    server=server,
                    scheduling_strategy=server.scheduling_strategy,
                    last_running=now(),
                    state=ROOM_STATE_ACTIVE,
                    is_breakout=breakout,
                )
                if event["attributes"]["meeting"]["metadata"].get("bbb-origin", None) == "Moodle":
                    room, created = ExternalRoom.objects.update_or_create(meeting_id=meeting_id, defaults=meta)
                else:
                    room, created = Room.objects.update_or_create(meeting_id=meeting_id, defaults=meta)
                logger.debug(
                    "room %s (name=%s, server=%s, scheduling_strategy=%s, is_breakout=%s)"
                    % (
                        "created" if created else "updated",
                        room.name,
                        room.server.dns,
                        room.scheduling_strategy.name,
                        room.is_breakout,
                    )
                )
            if event_id == "meeting-ended":
                room = Room.objects.get(meeting_id=meeting_id)
                logger.debug("meeting ended (domain=%s, id=%s, name=%s)" % (domain, room.meeting_id, room.name))
                if room.is_breakout or room.is_externalroom():
                    logger.info(
                        "room is of type %s and will be deleted (domain=%s, id=%s, name=%s)"
                        % ("breakout" if room.is_breakout else "external", domain, room.meeting_id, room.name)
                    )
                    room.delete()
                else:
                    logger.debug("room will be reset(domain=%s, id=%s, name=%s)" % (domain, room.meeting_id, room.name))
                    reset_room(room.meeting_id, room.name, room.config)

    logger.info("finished processing bbb callback request")
    return JsonResponse({"status": "ok"})


@csrf_exempt
def recording_callback(request):
    logger.info(create_view_access_logging_message(request))
    gp = GeneralParameter.load(get_current_site(request))
    data = json.loads(request.body)
    if hmac.compare_digest(data["token"].encode("UTF-8"), settings.RECORDINGS_SECRET.encode("UTF-8")):
        if Meeting.objects.filter(pk=data["rooms_meeting_id"]).update(
            replay_id=data["bbb_meeting_id"],
            replay_url=data.get("bbb_recordings_url", gp.playback_url + data["bbb_meeting_id"]),
        ):
            return HttpResponse(status=200)
        else:
            return HttpResponse(status=400)
    else:
        return HttpResponse(status=401)


@csrf_exempt
def api_server_registration(request):  # noqa C901 # McCabe can be a bad proxy for mental context at times
    if request.method != "POST":
        return HttpResponseNotAllowed(["POST"])
    if request.headers.get("Content-Type") != "application/json":
        return HttpResponse(status=415)
    if request.headers.get("X-Bbbatscale-Experimental-Api") != "iacceptmyfate":
        return HttpResponseBadRequest()

    # The request authentication scheme is as for webhooks with the added twist
    # that the MAC key is tenant-specific und thus needs to be looked up first
    # using data from the request.
    # The lookup key is the tenant name transmitted in the X-Bbbatscale-Tenant
    # header.
    sig = request.headers.get("X-Request-Signature")
    if not sig:
        return HttpResponseBadRequest()
    tenant_name = request.headers.get("X-Bbbatscale-Tenant")
    if not tenant_name:
        return HttpResponseBadRequest()
    try:
        timestamp, hex_mac = map(
            lambda x: x[1],
            sorted(filter(lambda x: x[0] in ["t", "v1"], map(lambda x: x.split("=", 1), sig.split(",")))),
        )
        timestamp_int = int(timestamp)
        request_mac = binascii.unhexlify(hex_mac)
        tenant = SchedulingStrategy.objects.get(name=tenant_name)
    except binascii.Error as e:
        logger.warning("received mis-encoded message authentication code: %s (X-Request-Signature: %s)", e, sig)
        return HttpResponseBadRequest()
    except (ValueError, KeyError) as e:
        logger.info("received bogus X-Request-Signature header: %s (value: %s)", e, sig)
        return HttpResponseBadRequest()
    except SchedulingStrategy.DoesNotExist:
        logger.warning("received server registration request for non-existent tenant %s", tenant_name)
        return HttpResponseBadRequest()

    mac_key = binascii.unhexlify(tenant.token_registration)
    msg = b"%s.%s" % (timestamp.encode("UTF-8"), request.body)
    expect_mac = hmac.digest(mac_key, msg, "SHA512")
    if not hmac.compare_digest(request_mac, expect_mac):
        return HttpResponse(status=403)

    # Allow for up to an hour of clock drift
    if abs(datetime.now().timestamp() - timestamp_int) > 3600:
        return HttpResponse(status=403)

    # When reaching this point, request.body was successfully authenticated
    # using the shared secret configured for tenant.
    data = json.loads(request.body)
    server, created = Server.objects.update_or_create(
        machine_id=data["machine_id"],
        defaults={
            "dns": data["hostname"],
            "shared_secret": data["shared_secret"],
            "scheduling_strategy": tenant,
        },
    )
    # For now just add the 'worker' type to all servers enrolled through this
    # endpoint. Going forward we may accept additional types taken from the
    # POSTed request.
    server.server_types.add(ServerType.objects.get_or_create(name="worker")[0])

    if created:
        return HttpResponse(status=201)

    return HttpResponse(status=204)


def bbb_initialization_request(request, scheduling_strategy_name):
    logger.info(create_view_access_logging_message(request, scheduling_strategy_name))
    try:
        SchedulingStrategy.objects.get(name=scheduling_strategy_name)
    except SchedulingStrategy.DoesNotExist:
        return bbb_xml_response("FAILED", "unknownResource", "Your requested resource is not available.")
    return HttpResponse(
        xmltodict.unparse({"response": {"returncode": "SUCCESS", "version": "2.0"}}), content_type="text/xml"
    )


@csrf_exempt
def bbb_api_create(request, scheduling_strategy_name):
    logger.info(create_view_access_logging_message(request, scheduling_strategy_name))
    body = request.body if request.method == "POST" else None
    # Create Meeting and return XML to moodle
    scheduling_strategy = SchedulingStrategy.objects.get(name=scheduling_strategy_name)
    params = request.GET.copy()
    if validate_bbb_api_call("create", params, scheduling_strategy):
        room = external_get_or_create_room_with_params(params, scheduling_strategy, tenant=get_current_site(request))
        if (
            ExternalRoom.objects.filter(meeting_id=params["meetingID"], state=ROOM_STATE_INACTIVE).update(
                state=ROOM_STATE_CREATING
            )
            >= 1
        ):
            response = meeting_create(request, params, room, body).text
            logger.debug(f"Creating meeting for {room.meeting_id}.")
            return HttpResponse(response, content_type="text/xml")
        logger.debug(f"Meeting for {room.meeting_id} already exists.")
        return bbb_xml_response(
            "SUCCESS", "duplicateWarning", "This conference was already in existence and may currently be in progress."
        )
    return bbb_xml_response("FAILED", "checksumError", "You did not pass the checksum security check")


def bbb_api_join(request, scheduling_strategy_name):
    logger.info(create_view_access_logging_message(request, scheduling_strategy_name))

    # Create Join URL and return to Moodle
    scheduling_strategy = SchedulingStrategy.objects.get(name=scheduling_strategy_name)
    params = request.GET.copy()
    if validate_bbb_api_call("join", params, scheduling_strategy):
        room = ExternalRoom.objects.filter(
            meeting_id=params["meetingID"],
            scheduling_strategy=scheduling_strategy,
            server__isnull=False,
            state=ROOM_STATE_ACTIVE,
        ).first()
        if room:
            bbb = BigBlueButton(room.server.dns, room.server.shared_secret)
            return redirect(bbb.join(params))
        logger.debug(f"No running meeting for {params['meetingID']} found.")
        return bbb_xml_response(
            "FAILED",
            "notFound",
            "We could not find a meeting with that meeting ID - perhaps the meeting is not yet running?",
        )
    return bbb_xml_response("FAILED", "checksumError", "You did not pass the checksum security check")


def bbb_api_end(request, scheduling_strategy_name):
    logger.info(create_view_access_logging_message(request, scheduling_strategy_name))
    # End Meeting
    scheduling_strategy = SchedulingStrategy.objects.get(name=scheduling_strategy_name)
    params = request.GET.copy()
    if validate_bbb_api_call("end", params, scheduling_strategy):
        room = ExternalRoom.objects.filter(
            meeting_id=params["meetingID"],
            scheduling_strategy=scheduling_strategy,
            server__isnull=False,
            state=ROOM_STATE_ACTIVE,
        ).first()
        if room:
            bbb = BigBlueButton(room.server.dns, room.server.shared_secret)
            response = bbb.end(room.meeting_id, room.moderator_pw)
            logger.debug(f"Meeting {params['meetingID']} ended with api call.")
            return HttpResponse(response.text, content_type="text/xml")
        logger.debug(f"No running meeting for {params['meetingID']} found.")
        return bbb_xml_response(
            "FAILED",
            "notFound",
            "We could not find a meeting with that meeting ID - perhaps the meeting is not yet running?",
        )
    return bbb_xml_response("FAILED", "checksumError", "You did not pass the checksum security check")


def bbb_api_is_meeting_running(request, scheduling_strategy_name):
    logger.info(create_view_access_logging_message(request, scheduling_strategy_name))
    # Is meeting running
    scheduling_strategy = SchedulingStrategy.objects.get(name=scheduling_strategy_name)
    params = request.GET.copy()
    if validate_bbb_api_call("isMeetingRunning", params, scheduling_strategy):
        room = ExternalRoom.objects.filter(
            meeting_id=params["meetingID"],
            scheduling_strategy=scheduling_strategy,
            server__isnull=False,
            state=ROOM_STATE_ACTIVE,
        ).first()
        if room:
            bbb = BigBlueButton(room.server.dns, room.server.shared_secret)
            response = bbb.is_meeting_running(room.meeting_id)
            return HttpResponse(response.text, content_type="text/xml")
        return bbb_xml_meeting_running_false()
    return bbb_xml_response("FAILED", "checksumError", "You did not pass the checksum security check")


def bbb_api_get_meeting_info(request, scheduling_strategy_name):
    logger.info(create_view_access_logging_message(request, scheduling_strategy_name))
    # Get Meeting Infos and return XML
    scheduling_strategy = SchedulingStrategy.objects.get(name=scheduling_strategy_name)
    params = request.GET.copy()
    if validate_bbb_api_call("getMeetingInfo", params, scheduling_strategy):
        room = ExternalRoom.objects.filter(
            meeting_id=params["meetingID"],
            scheduling_strategy=scheduling_strategy,
            server__isnull=False,
            state=ROOM_STATE_ACTIVE,
        ).first()
        if room:
            bbb = BigBlueButton(room.server.dns, room.server.shared_secret)
            response = bbb.get_meeting_infos(room.meeting_id)
            return HttpResponse(response.text, content_type="text/xml")
        logger.debug(f"No running meeting for {params['meetingID']} found.")
        return bbb_xml_response("FAILED", "notFound", "We could not find a meeting with that meeting ID")
    return bbb_xml_response("FAILED", "checksumError", "You did not pass the checksum security check")


def bbb_api_get_recordings(request, scheduling_strategy_name):
    logger.info(create_view_access_logging_message(request, scheduling_strategy_name))

    scheduling_strategy = SchedulingStrategy.objects.get(name=scheduling_strategy_name)
    params = request.GET.copy()
    if validate_bbb_api_call("getRecordings", params, scheduling_strategy):
        servers = Server.objects.filter(
            scheduling_strategy=scheduling_strategy,
            server_types__in=[ServerType.objects.get(name="playback").pk],
            state=SERVER_STATE_UP,
        )
        recording_response = parse_recordings_from_multiple_sources(servers, params)
        if recording_response:
            return HttpResponse(parse_dict_to_xml(recording_response), content_type="text/xml")
        return bbb_xml_response(
            "SUCCESS", "noRecordings", "There are no recordings for the meeting(s).", recordings=None
        )
    return bbb_xml_response("FAILED", "checksumError", "You did not pass the checksum security check")


def bbb_api_publish_recordings(request, scheduling_strategy_name):
    logger.info(create_view_access_logging_message(request, scheduling_strategy_name))

    scheduling_strategy = SchedulingStrategy.objects.get(name=scheduling_strategy_name)
    params = request.GET.copy()
    if validate_bbb_api_call("publishRecordings", params, scheduling_strategy):
        if Meeting.objects.filter(replay_id=params["recordID"]).exists():
            servers = Server.objects.filter(
                scheduling_strategy=scheduling_strategy,
                server_types__in=[ServerType.objects.get(name="playback").pk],
                state=SERVER_STATE_UP,
            )
            response = publish_recordings(servers, params)
            if response:
                return HttpResponse(response.text, content_type="text/xml")
        return bbb_xml_response("FAILED", "notFound", "We could not find recordings")
    return bbb_xml_response("FAILED", "checksumError", "You did not pass the checksum security check")


def bbb_api_update_recordings(request, scheduling_strategy_name):
    logger.info(create_view_access_logging_message(request, scheduling_strategy_name))

    scheduling_strategy = SchedulingStrategy.objects.get(name=scheduling_strategy_name)
    params = request.GET.copy()
    if validate_bbb_api_call("updateRecordings", params, scheduling_strategy):
        if Meeting.objects.filter(replay_id=params["recordID"]).exists():
            servers = Server.objects.filter(
                scheduling_strategy=scheduling_strategy,
                server_types__in=[ServerType.objects.get(name="playback").pk],
                state=SERVER_STATE_UP,
            )
            response = update_recordings(servers, params)
            if response:
                return HttpResponse(response.text, content_type="text/xml")
        return bbb_xml_response("FAILED", "notFound", "We could not find recordings")
    return bbb_xml_response("FAILED", "checksumError", "You did not pass the checksum security check")


def bbb_api_delete_recordings(request, scheduling_strategy_name):
    logger.info(create_view_access_logging_message(request, scheduling_strategy_name))

    scheduling_strategy = SchedulingStrategy.objects.get(name=scheduling_strategy_name)
    params = request.GET.copy()
    if validate_bbb_api_call("deleteRecordings", params, scheduling_strategy):
        if Meeting.objects.filter(replay_id=params["recordID"]).exists():
            servers = Server.objects.filter(
                scheduling_strategy=scheduling_strategy,
                server_types__in=[ServerType.objects.get(name="playback").pk],
                state=SERVER_STATE_UP,
            )
            response = delete_recordings(servers, params)
            if response:
                return HttpResponse(response.text, content_type="text/xml")
        return bbb_xml_response("FAILED", "notFound", "We could not find recordings")
    return bbb_xml_response("FAILED", "checksumError", "You did not pass the checksum security check")


def translate_bbb_meta_data(metadata):
    try:
        logger.debug(f"Translation of metadata: {metadata}")
        return {
            "mute_on_start": metadata["muteonstart"] == "True",
            "moderation_mode": metadata["moderationmode"],
            "guest_policy": metadata["guestpolicy"],
            "allow_guest_entry": metadata["allowguestentry"] == "True",
            "access_code": metadata["accesscode"] if metadata["accesscode"] != "None" else None,
            "only_prompt_guests_for_access_code": metadata["onlypromptguestsforaccesscode"] == "True",
            "disable_cam": metadata["disablecam"] == "True",
            "disable_mic": metadata["disablemic"] == "True",
            "disable_note": metadata["disablenote"] == "True",
            "disable_private_chat": metadata["disableprivatechat"] == "True",
            "disable_public_chat": metadata["disablepublicchat"] == "True",
            "allow_recording": metadata["allowrecording"] == "True",
            "logoutUrl": metadata["logouturl"],
            "maxParticipants": metadata["maxparticipants"] if metadata["maxparticipants"] != "None" else None,
            "streamingUrl": metadata["streamingurl"] if metadata["streamingurl"] != "None" else None,
        }
    except KeyError as e:
        logger.warning(f"Key error while translating metadata: {str(e)}")
        return {}


def bbb_xml_response(returncode, messageKey, message, **kwargs):
    return HttpResponse(
        xmltodict.unparse(
            {
                "response": {
                    "returncode": returncode,
                    **kwargs,
                    "messageKey": messageKey,
                    "message": message,
                }
            }
        ),
        content_type="text/xml",
    )


def bbb_xml_meeting_running_false(running=False):
    return HttpResponse(
        xmltodict.unparse({"response": {"returncode": "SUCCESS", "running": "false" if not running else "true"}}),
        content_type="text/xml",
    )
