import logging

from core.filters import SchedulingStrategyFilter
from core.forms import SchedulingStrategyFilterFormHelper, SchedulingStrategyForm
from core.models import SchedulingStrategy
from core.views import create_view_access_logging_message
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


@login_required
@staff_member_required
def scheduling_strategy_overview(request):
    logger.info(create_view_access_logging_message(request))

    f = SchedulingStrategyFilter(request.GET, queryset=SchedulingStrategy.objects.all().prefetch_related("tenants"))
    f.form.helper = SchedulingStrategyFilterFormHelper
    return render(request, "scheduling_strategy_overview.html", context={"filter": f})


@login_required
@staff_member_required
def scheduling_strategy_create(request):
    logger.info(create_view_access_logging_message(request))

    form = SchedulingStrategyForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        scheduling_strategy = form.save()

        messages.success(request, _("Scheduling Strategy was created successfully."))
        logger.debug("Scheduling Strategy (%s) was created successfully.", scheduling_strategy)
        # return to URL
        return redirect("scheduling_strategy_overview")

    logger.debug("Scheduling Strategy (%s) was not updated correctly; is_valid=%s", form.is_valid())
    return render(request, "scheduling_strategy_create.html", {"form": form})


@login_required
@staff_member_required
def scheduling_strategy_delete(request, scheduling_strategy):
    logger.info(create_view_access_logging_message(request, scheduling_strategy))

    instance = get_object_or_404(SchedulingStrategy, pk=scheduling_strategy)
    instance.delete()
    messages.success(request, _("Scheduling Strategy was deleted successfully."))
    logger.debug("Scheduling Strategy (%s) was deleted successfully", scheduling_strategy)

    return redirect("scheduling_strategy_overview")


@login_required
@staff_member_required
def scheduling_strategy_update(request, scheduling_strategy):
    logger.info(create_view_access_logging_message(request, scheduling_strategy))

    instance = get_object_or_404(SchedulingStrategy, pk=scheduling_strategy)
    form = SchedulingStrategyForm(request.POST or None, instance=instance)
    if request.method == "POST" and form.is_valid():
        _scheduling_strategy = form.save()
        messages.success(request, _("Scheduling Strategy was updated successfully."))
        logger.debug("Scheduling Strategy (%s) was updated successfully", _scheduling_strategy)

        return redirect("scheduling_strategy_overview")

    logger.debug("Scheduling Strategy (%s) was NOT updated successfully", instance)
    return render(request, "scheduling_strategy_create.html", {"form": form})
