import pytest
from core.models import Room, SchedulingStrategy, Server, get_default_room_config
from core.services import room_click


@pytest.fixture(scope="function")
def example(db) -> SchedulingStrategy:
    return SchedulingStrategy.objects.create(
        name="example",
    )


@pytest.fixture(scope="function")
def example_server(db, example) -> Server:
    return Server.objects.create(
        scheduling_strategy=example,
        dns="example.org",
        shared_secret="123456789",
    )


@pytest.fixture(scope="function")
def room_d14_0303_qis(db, example, example_server) -> Room:
    return Room.objects.create(
        scheduling_strategy=example,
        server=example_server,
        name="room_every_one_can_start",
        participant_count=10,
        videostream_count=5,
        event_collection_strategy="QISICalCollector",
        event_collection_parameters='{"qis_url": "qis.example.org", ' '"qis_id": "118", ' '"qis_encoding": "UTF-8"}',
        config=get_default_room_config(),
    )


def test_room_click(room_d14_0303_qis):
    room_click(room_d14_0303_qis.pk)
    room_click(room_d14_0303_qis.pk)
    room_click(room_d14_0303_qis.pk)

    click_counter = Room.objects.get(pk=room_d14_0303_qis.pk).click_counter

    assert click_counter == 3
