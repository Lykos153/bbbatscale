import pytest
from core.constants import MODERATION_MODE_MODERATORS
from core.models import ExternalRoom, Room, SchedulingStrategy, get_default_room_config
from core.services import create_meeting_and_bbb_parameter
from django.http import HttpRequest


@pytest.fixture(scope="function")
def example(db) -> SchedulingStrategy:
    return SchedulingStrategy.objects.create(
        name="example",
    )


@pytest.fixture(scope="function")
def create_parameter_external_room(db):
    return {
        "name": "sample_create_parameter_name",
        "meeting_id": "123-456-789",
        "attendee_pw": "test_attendee_pw",
        "moderator_pw": "test_moderator_pw",
        "mute_on_start": "True",
        "record": "true",
        "welcome_message": None,
        "dialNumber": None,
    }


@pytest.fixture(scope="function")
def wrong_create_parameters(db):
    return {
        "_name_": "wrong_param_name",
        "_meetingID_": "123-456-789",
        "_attendeePW_": "test_attendee_pw",
        "_moderator_pw_": "test_moderator_pw",
        "_mute_on_start": "True",
    }


@pytest.fixture(scope="function")
def create_parameter_not_external_room(db):
    return {
        "name": "sample_create_parameter_name",
        "meeting_id": "123-456-789",
        "attendee_pw": "test_attendee_pw",
        "moderator_pw": "test_moderator_pw",
        "logoutUrl": "testserver",
        "mute_on_start": True,
        "access_code": "123",
        "only_prompt_guests_for_access_code": True,
        "guest_policy": "ASK_MODERATOR",
        "disable_cam": True,
        "disable_mic": True,
        "disable_note": True,
        "disable_public_chat": True,
        "disable_private_chat": True,
        "allow_recording": True,
        "creator": "sample_meeting_creator",
        "moderation_mode": MODERATION_MODE_MODERATORS,
        "allow_guest_entry": True,
        "welcome_message": None,
        "dialNumber": None,
        "maxParticipants": None,
        "streamingUrl": "rtmp://test-stream-url",
    }


@pytest.fixture(scope="function")
def external_room(db, example) -> ExternalRoom:
    return ExternalRoom.objects.create(
        scheduling_strategy=example,
        meeting_id="99999999",
        name="D14/02.14",
        attendee_pw="test_attendee_password_external",
        moderator_pw="test_moderator_password_external",
        moderation_mode=MODERATION_MODE_MODERATORS,
        config=get_default_room_config(),
    )


@pytest.fixture(scope="function")
def no_external_room(db, example) -> Room:
    return Room.objects.create(
        scheduling_strategy=example,
        meeting_id="1234567891011",
        name="not_external_room",
        attendee_pw="test_attendee_password",
        moderator_pw="test_moderator_password",
        moderation_mode=MODERATION_MODE_MODERATORS,
        everyone_can_start=True,
        config=get_default_room_config(),
    )


def test_create_meeting_and_bbb_parameter_external_room(
    create_parameter_external_room,
    wrong_create_parameters,
    create_parameter_not_external_room,
    external_room,
    no_external_room,
):
    request = HttpRequest()
    request.META["HTTP_HOST"] = "testserver"

    # if room is external room
    assert (
        create_meeting_and_bbb_parameter(request, external_room, create_parameter_external_room)["name"]
        == "sample_create_parameter_name"
    )
    assert (
        create_meeting_and_bbb_parameter(request, external_room, create_parameter_external_room)["meeting_id"]
        == "123-456-789"
    )
    assert (
        create_meeting_and_bbb_parameter(request, external_room, create_parameter_external_room)["meta_creator"]
        == "External"
    )
    assert create_meeting_and_bbb_parameter(request, external_room, create_parameter_external_room)[
        "meta_allowrecording"
    ]
    assert (
        create_meeting_and_bbb_parameter(request, external_room, create_parameter_external_room)["attendee_pw"]
        == "test_attendee_pw"
    )
    assert (
        create_meeting_and_bbb_parameter(request, external_room, create_parameter_external_room)["moderator_pw"]
        == "test_moderator_pw"
    )
    assert create_meeting_and_bbb_parameter(request, external_room, create_parameter_external_room)["mute_on_start"]
    assert create_meeting_and_bbb_parameter(request, external_room, create_parameter_external_room)["record"]
    assert (
        create_meeting_and_bbb_parameter(request, external_room, create_parameter_external_room)["welcome_message"]
        is None
    )
    assert (
        create_meeting_and_bbb_parameter(request, external_room, create_parameter_external_room)["dialNumber"] is None
    )

    # if passed parameters are wrong
    assert create_meeting_and_bbb_parameter(request, external_room, wrong_create_parameters) is None

    # if room is not external room
    assert (
        create_meeting_and_bbb_parameter(request, no_external_room, create_parameter_not_external_room)["name"]
        == "sample_create_parameter_name"
    )
    assert (
        create_meeting_and_bbb_parameter(request, no_external_room, create_parameter_not_external_room)["meetingID"]
        == "123-456-789"
    )
    assert (
        create_meeting_and_bbb_parameter(request, no_external_room, create_parameter_not_external_room)["attendeePW"]
        == "test_attendee_pw"
    )
    assert (
        create_meeting_and_bbb_parameter(request, no_external_room, create_parameter_not_external_room)["moderatorPW"]
        == "test_moderator_pw"
    )
    assert (
        create_meeting_and_bbb_parameter(request, no_external_room, create_parameter_not_external_room)["muteOnStart"]
        == "true"
    )
    assert (
        create_meeting_and_bbb_parameter(request, no_external_room, create_parameter_not_external_room)[
            "lockSettingsDisableCam"
        ]
        == "true"
    )
    assert (
        create_meeting_and_bbb_parameter(request, no_external_room, create_parameter_not_external_room)[
            "lockSettingsDisableMic"
        ]
        == "true"
    )
    assert (
        create_meeting_and_bbb_parameter(request, no_external_room, create_parameter_not_external_room)[
            "lockSettingsDisableNote"
        ]
        == "true"
    )
    assert (
        create_meeting_and_bbb_parameter(request, no_external_room, create_parameter_not_external_room)[
            "lockSettingsDisablePublicChat"
        ]
        == "true"
    )
    assert (
        create_meeting_and_bbb_parameter(request, no_external_room, create_parameter_not_external_room)[
            "lockSettingsDisablePrivateChat"
        ]
        == "true"
    )
    assert (
        create_meeting_and_bbb_parameter(request, no_external_room, create_parameter_not_external_room)["guestPolicy"]
        == "ASK_MODERATOR"
    )
    assert (
        create_meeting_and_bbb_parameter(request, no_external_room, create_parameter_not_external_room)["record"]
        == "true"
    )
    assert (
        create_meeting_and_bbb_parameter(request, no_external_room, create_parameter_not_external_room)[
            "allowStartStopRecording"
        ]
        == "true"
    )
    assert (
        create_meeting_and_bbb_parameter(request, no_external_room, create_parameter_not_external_room)["meta_creator"]
        == "sample_meeting_creator"
    )
    assert create_meeting_and_bbb_parameter(request, no_external_room, create_parameter_not_external_room)[
        "meta_muteonstart"
    ]
    assert (
        create_meeting_and_bbb_parameter(request, no_external_room, create_parameter_not_external_room)[
            "meta_moderationmode"
        ]
        == MODERATION_MODE_MODERATORS
    )
    assert (
        create_meeting_and_bbb_parameter(request, no_external_room, create_parameter_not_external_room)[
            "meta_guestpolicy"
        ]
        == "ASK_MODERATOR"
    )
    assert create_meeting_and_bbb_parameter(request, no_external_room, create_parameter_not_external_room)[
        "meta_allowguestentry"
    ]
    assert (
        create_meeting_and_bbb_parameter(request, no_external_room, create_parameter_not_external_room)[
            "meta_accesscode"
        ]
        == "123"
    )
    assert create_meeting_and_bbb_parameter(request, no_external_room, create_parameter_not_external_room)[
        "meta_onlypromptguestsforaccesscode"
    ]
    assert create_meeting_and_bbb_parameter(request, no_external_room, create_parameter_not_external_room)[
        "meta_disablecam"
    ]
    assert create_meeting_and_bbb_parameter(request, no_external_room, create_parameter_not_external_room)[
        "meta_disablemic"
    ]
    assert create_meeting_and_bbb_parameter(request, no_external_room, create_parameter_not_external_room)[
        "meta_disablenote"
    ]
    assert create_meeting_and_bbb_parameter(request, no_external_room, create_parameter_not_external_room)[
        "meta_disableprivatechat"
    ]
    assert create_meeting_and_bbb_parameter(request, no_external_room, create_parameter_not_external_room)[
        "meta_disablepublicchat"
    ]
    assert create_meeting_and_bbb_parameter(request, no_external_room, create_parameter_not_external_room)[
        "meta_allowrecording"
    ]
    assert create_meeting_and_bbb_parameter(request, no_external_room, create_parameter_not_external_room)[
        "meta_streamingUrl"
    ]
    assert (
        create_meeting_and_bbb_parameter(request, no_external_room, create_parameter_not_external_room)[
            "meta_logouturl"
        ]
        == "testserver"
    )

    create_parameter_not_external_room["streamingUrl"] = None
    assert (
        create_meeting_and_bbb_parameter(request, no_external_room, create_parameter_not_external_room)[
            "meta_streamingUrl"
        ]
        is None
    )
