import pytest
from core.models import Room, SchedulingStrategy, Server, get_default_room_config
from core.services import meeting_end


@pytest.fixture(scope="function")
def example(db) -> SchedulingStrategy:
    return SchedulingStrategy.objects.create(
        name="example",
    )


@pytest.fixture(scope="function")
def example_server(db, example) -> Server:
    return Server.objects.create(
        scheduling_strategy=example,
        dns="example.org",
        shared_secret="123456789",
    )


@pytest.fixture(scope="function")
def room_d14_0204(db, example, example_server) -> Room:
    return Room.objects.create(
        scheduling_strategy=example,
        server=example_server,
        meeting_id="123-312-456-654",
        name="D14/02.04",
        config=get_default_room_config(),
    )


@pytest.mark.django_db
def test_meeting_end(room_d14_0204, mocker):
    mocker.patch("core.utils.BigBlueButton.end", return_value="https://example.org/bigbluebutton/api/")
    meet_end = meeting_end("123-312-456-654", "test_password")

    assert meet_end == "https://example.org/bigbluebutton/api/"
