from core.models import GeneralParameter
from django import template
from django.contrib.sites.shortcuts import get_current_site
from django.template import RequestContext
from django.template.base import FilterExpression, Node, Parser, Token

register = template.Library()


class ThemeNode(Node):
    def __init__(self, theme_element: FilterExpression, wrapper_type: FilterExpression) -> None:
        self.theme_element = theme_element
        self.wrapper_type = wrapper_type

    def render(self, context: RequestContext) -> str:
        user = context["user"]
        tenant = get_current_site(context["request"])
        theme_element = self.theme_element.resolve(context)
        wrapper_type = self.wrapper_type.resolve(context)

        if wrapper_type.lower() == "stylesheet":
            wrapper = '<link rel="stylesheet" href="%s">'
            theme_element += "_stylesheet"
        elif wrapper_type.lower() == "script":
            wrapper = '<script src="%s"></script>'
            theme_element += "_script"
        else:
            raise template.TemplateSyntaxError(f"Unknown type '{wrapper_type}'.")

        if user.is_authenticated:
            css_path = getattr(user.get_theme(), theme_element)
        else:
            css_path = getattr(GeneralParameter.load(tenant).default_theme, theme_element)

        if css_path:
            return wrapper % css_path
        else:
            return ""


@register.tag("theme")
def do_theme(parser: Parser, token: Token) -> ThemeNode:
    bits = token.split_contents()

    if len(bits) != 3:
        raise template.TemplateSyntaxError(f"Tag '{bits[0]}' expects exactly two arguments.")

    theme_element = parser.compile_filter(bits[1])
    wrapper_type = parser.compile_filter(bits[2])

    return ThemeNode(theme_element, wrapper_type)
