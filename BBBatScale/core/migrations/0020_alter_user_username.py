# Generated by Django 3.2.2 on 2021-05-12 14:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("core", "0019_auto_20210504_2132"),
    ]

    operations = [
        migrations.AlterField(
            model_name="user",
            name="username",
            field=models.CharField(
                error_messages={"unique": "A user with that username already exists."},
                help_text="Required. 511 characters or fewer. Letters, digits and @/./+/-/_/= only.",
                max_length=511,
                unique=True,
                verbose_name="username",
            ),
        ),
    ]
