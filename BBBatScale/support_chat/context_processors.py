from typing import Dict

from django.contrib.sites.shortcuts import get_current_site
from django.http import HttpRequest
from support_chat.models import SupportChatParameter


def support_chat_parameter(request: HttpRequest) -> Dict[str, SupportChatParameter]:
    _tenant = get_current_site(request)
    return {"support_chat_parameter": SupportChatParameter.load(_tenant)}
