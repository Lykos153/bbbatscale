/**
 * Creates a URL with the following rules:
 * - If a path contains a host (starting with a protocol or double slash),
 *   the following paths are relative to the path containing the host, previous ones will be discarded,
 *   otherwise the paths will be relative to the current [host]{@link Location.host}.
 * - If a (following) path is absolute, the previous paths (not the host) will be discarded
 *   and the absolute is the new path base.
 * - If the paths do not contain any absolute paths or paths containing hosts,
 *   the current [location]{@link Location} will be used as the base including the [path]{@link Location.path}.
 *   If the last [path]{@link Location.path} element does not end with a slash, this path element will be discarded.
 *
 * @param {URL, string} base
 * @param {...string} paths
 * @return {URL}
 */
function getUrl(base, ...paths) {
    let url = new URL(base, window.location);
    for (const path of paths) {
        if (!url.pathname.endsWith('/')) {
            url.pathname += '/';
        }
        url = new URL(path, url);
    }
    return url;
}

/**
 * Creates a new WebSocket object with the supplied path(s).
 * If the used [protocol]{@link Location.protocol} is not http or ws, the WebSocket will be encrypted.
 *
 * @param {URL, string} base The base path to connect the WebSocket to.
 * @param {...string} paths The sub paths appended to the base.
 * @see getUrl More information about the url creation.
 *
 * @return {WebSocket} The newly created WebSocket.
 */
function createWebSocket(base, ...paths) {
    const url = getUrl(base, ...paths);
    if (window.location.protocol === 'http:' || window.location.protocol === 'ws:') {
        url.protocol = 'ws:';
    } else {
        url.protocol = 'wss:';
    }
    return new WebSocket(url.href);
}

/**
 * Adds the listener to the supplied subject without removing the previous one, if any.
 * The listener will be installed at 'subject[listenerName]'.
 *
 * @param {Object} subject
 * @param {string} listenerName
 * @param {function} listenerFunction
 */
function addListener(subject, listenerName, listenerFunction) {
    const previousListener = subject[listenerName];
    subject[listenerName] = function (...args) {
        if (previousListener) {
            previousListener(...args);
        }
        listenerFunction(...args);
    }
}

const DATE_TIME_FORMAT = new Intl.DateTimeFormat(undefined, {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric'
});

const TIME_FORMAT = new Intl.DateTimeFormat(undefined, {
    hour: 'numeric',
    minute: 'numeric'
});

/**
 * Checks if the supplied date is today.
 *
 * @param {Date} date The date to check.
 * @return {boolean} True if date is today, false otherwise.
 */
function isToday(date) {
    const today = new Date();
    return date.getDate() === today.getDate()
        && date.getMonth() === today.getMonth()
        && date.getFullYear() === today.getFullYear();
}

/**
 * Formats the supplied date.
 * Uses {@link TIME_FORMAT} to format the timestamp in case it is today, {@link DATE_TIME_FORMAT} otherwise.
 *
 * @param {Date} date The date to format.
 * @return {string} The formatted date.
 */
function formatDate(date) {
    if (isToday(date)) {
        return TIME_FORMAT.format(date);
    } else {
        return DATE_TIME_FORMAT.format(date);
    }
}

/**
 * Copy the specified text to the clipboard and returns a Promise.
 * If the text has successfully been copied to the clipboard the Promise will be 'fulfilled',
 * otherwise the Promise will be 'rejected'.
 *
 * @param {string} text
 * @return {Promise<void>}
 */
function copyToClipboard(text) {
    return new Promise(function (resolve, reject) {

        function copyToClipboardFallback() {
            if (document.queryCommandSupported('copy')) {
                const input = document.createElement('input');
                input.hidden = true;
                input.style.display = 'none';
                input.type = 'text';
                input.value = text;
                document.body.appendChild(input);

                input.select();
                input.setSelectionRange(0, text.length);
                const result = document.execCommand('copy');
                input.remove();

                if (result) {
                    resolve();
                } else {
                    reject();
                }
            } else {
                reject();
            }
        }

        if (navigator.clipboard) {
            navigator.clipboard.writeText(text).then(() => {
                resolve();
            }, copyToClipboardFallback);
        } else {
            copyToClipboardFallback(text);
        }
    });
}

/**
 * Copies the supplied URL to the clipboard and presents a toast with the success of this operation.
 *
 * @param {string} url a full or relative URL
 * @return {Promise<void>}
 */
function copyURLToClipboard(url) {
    return copyToClipboard(new URL(url, window.location).toString()).then(function() {
        toastr.success(gettext("The URL has successfully been copied to the clipboard."));
    }, function() {
        toastr.error(gettext("Your Browser does not support copying to the clipboard."));
    });
}

/**
 * Create a new sorted list.
 *
 * @class
 * @template K
 * @template E
 * @param {function(E, (E|K)): number} comparator
 * @param {function(E, E): boolean} equal
 * @return {SortedList<K, E>}
 */
function SortedList(comparator, equal) {
    if (!(this instanceof SortedList)) {
        return new SortedList(comparator, equal);
    }

    /**
     * @type {E[]}
     */
    let list = [];

    /**
     * @param {E|K} key
     * @return {number}
     */
    function binarySearch(key) {
        let low = 0;
        let high = list.length - 1;

        while (low <= high) {
            const mid = Math.floor((low + high) / 2);
            const midVal = list[mid];

            const cmp = comparator(midVal, key);
            if (cmp < 0) {
                low = mid + 1;
            } else if (cmp > 0) {
                high = mid - 1;
            } else {
                return mid;
            }
        }

        return -(low + 1);
    }

    /**
     * @param {E|K} key
     * @return {number}
     */
    function binarySearchFirst(key) {
        let index = binarySearch(key);

        if (index >= 0) {
            while (index > 0 && comparator(list[index - 1], key) === 0) {
                index--;
            }
        }

        return index;
    }

    /**
     * @param {E|K} key
     * @return {number}
     */
    function binarySearchLast(key) {
        let index = binarySearch(key);

        if (index >= 0) {
            index++;
            while (index < list.length && comparator(list[index], key) === 0) {
                index++;
            }
        }

        return index;
    }

    /**
     * @param {E} element
     * @return {number}
     */
    function binarySearchFirstEqual(element) {
        const firstIndex = binarySearchFirst(element);
        let index = firstIndex;

        if (index >= 0) {
            if (equal(list[index], element)) {
                return index;
            }
            index++;

            while (index < list.length && comparator(list[index], element) === 0) {
                if (equal(list[index], element)) {
                    return index;
                }
                index++;
            }

            index = -(firstIndex + 1);
        }

        return index;
    }

    /**
     * @return {number} The number of elements in this list.
     */
    this.size = function () {
        return list.length;
    };

    /**
     * @return {boolean} True if the list contains no elements, false otherwise.
     */
    this.empty = function () {
        return list.length === 0;
    };

    /**
     * Returns true if at least one element of this list matches the supplied `key`
     * regarding the at initialization supplied `comparator` function.
     *
     * @param {K} key
     * @return {boolean} True if the key matches at least one element, false otherwise.
     */
    this.contains = function (key) {
        return binarySearch(key) >= 0;
    };

    /**
     * Returns true if at least one element of this list matches the supplied `element`
     * regarding the at initialization supplied `equal` function.
     *
     * @param {E} element
     * @return {boolean} True if the element equals at least one element, false otherwise.
     */
    this.containsElement = function (element) {
        return binarySearchFirstEqual(element) >= 0;
    };

    /**
     * @param {number} index
     * @return {?E} The element at the specified index. If the index is negative,
     * the list starts counting from the end (e.g. -1 would be the last element, -2 the second to last, ...).
     * If the index is out of bounds, null will be returned.
     */
    this.get = function (index) {
        if (index < 0) {
            index = list.length + index;
        }

        if (index >= 0 && index < list.length) {
            return list[index];
        }
        return null;
    };

    /**
     * @param {E|K} key
     * @return {?E[]}
     */
    this.find = function (key) {
        const startIndex = binarySearchFirst(key);
        let endIndex = startIndex + 1;

        if (startIndex >= 0) {
            while (endIndex < list.length && comparator(list[endIndex], key) === 0) {
                endIndex++;
            }

            return list.slice(startIndex, endIndex);
        }
        return null;
    };

    /**
     * Performs the specified callback for each element in an array.
     * The supplied callback should receive exactly one argument.
     * This callback function will be called with the respective element for each element of this list.
     *
     * @param {function(E): void} callback
     * @param {any} [thisArg=undefined] An object to which the this keyword can refer in the callback function.
     * If thisArg is omitted, `undefined` is used as the this value.
     */
    this.forEach = function (callback, thisArg = undefined) {
        list.forEach(function (element) {
            callback.call(thisArg, element);
        });
    };

    /**
     * @param {E} element
     * @return {number} The index at which the element has been inserted.
     */
    this.add = function (element) {
        let index = binarySearchLast(element);
        if (index < 0) {
            index = -(index + 1);
        }

        if (index === list.length) {
            list.push(element);
        } else {
            list.splice(index, 0, element);
        }
        return index;
    };

    /**
     * Adds the specified element, but only if it did not already exist.
     *
     * @param {E} element
     * @return {?number} The index at which the element has been inserted or null if it already existed.
     */
    this.ensureExists = function (element) {
        let index = binarySearchFirstEqual(element);
        if (index < 0) {
            index = -(index + 1);
        } else {
            return null;
        }

        if (index === list.length) {
            list.push(element);
        } else {
            list.splice(index, 0, element);
        }
        return index;
    };

    /**
     * Removes the first exact match.
     *
     * @param {E} element
     * @return {boolean} True if one element has been removed, false otherwise.
     */
    this.remove = function (element) {
        const index = binarySearchFirstEqual(element);

        if (index >= 0) {
            list.splice(index, 1);
            return true;
        }
        return false;
    };

    /**
     * Removes all elements from the list.
     */
    this.clear = function () {
        list = [];
    };
}
